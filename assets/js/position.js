function new_jabatan(){
	$.ajax({
	  method: "POST",
	  url: base_url + "pegawai/JabatanController/add_jabatan",
	  data: new FormData($('#form-jabatan')[0]),
	  processData: false,
        contentType: false,
	  success: function(response){
	  	if (response == 'berhasil') {
	  		$('#modal-jabatan').modal('hide');
	  		$("#form-jabatan")[0].reset();
	  		swal("Berhasil menambah data").then((value) => {
			 	// location.reload();
			 	list_jabatan();
			});
	  	}else{
	  		toastr.warning(response);
	  	}
	  }
	});
}

function edit_jabatan(){
	$.ajax({
	  method: "POST",
	  url: base_url + "pegawai/JabatanController/edit_jabatan",
	  data: new FormData($('#form-edit-jabatan')[0]),
	  processData: false,
        contentType: false,
	  success: function(response){
	  	if (response == 'berhasil') {
	  		$('#modal-edit-jabatan').modal('hide');
	  		$("#form-edit-jabatan")[0].reset();
	  		swal("Berhasil mengubah data").then((value) => {
			 	// location.reload();
			 	list_jabatan();
			});
	  	}else{
	  		toastr.warning(response);
	  	}
	  }
	});
}

function list_jabatan() {
    tabel_jabatan.clear().draw()
    $.ajax({
        url: base_url + "pegawai/JabatanController/get_jabatan",
        type: 'get',
        cache: false,
        //async: false,
        success: function(response, status, error, pesan) {
            response = $.parseJSON(response);
            var i = 1;
            var aksi = "<a class='green' href='#!' ><i class='ace-icon fa fa-pencil bigger-130 col-sm-2 dtl-jabatan'></i></a><a class='red' href='#!'><i class='ace-icon fa fa-trash-o bigger-130 col-sm-2 delete-jabatan'></i></a>";
            $.each(response, function() {
                tabel_jabatan.row.add([
                    i,
                    this['name'],
                    aksi,
                    this['id']
                ]).draw(false);
                i++;
            });

        },
        error: function(response) {
            console.log(response['responseText']);
        }
    });
}

function delete_jabatan(id){
	$.ajax({
	  method: "POST",
	  url: base_url + "pegawai/JabatanController/delete_jabatan",
	  data: { 
		id: id
	  },
	  success: function(response){
	  	if (response == 'berhasil') {
	  		swal("Berhasil Menghapus Data").then((value) => {
			 	list_jabatan();
			});
	  	}else{
	  		toastr.warning(response);
	  	}
	  }
	});
}

var tabel_jabatan = $('#tabel-position').DataTable({});

if(controller == 'JabatanController'){
	list_jabatan();
}

$(document).ready(function(){
	$('#save-jabatan').click(function(e){
		new_jabatan();
		e.preventDefault();
	});

	$('#edit-jabatan').click(function(e){
		edit_jabatan();
		e.preventDefault();
	});

	$('#tabel-position').on('click', '.delete-jabatan, .dtl-jabatan', function(){
		if (typeof tabel_jabatan.row($(this).closest('tr')).data() !== 'undefined') {
            arr_data_jabatan = tabel_jabatan.row($(this).closest('tr')).data();
        } else if (typeof tabel_jabatan.row(this).data() !== 'undefined') {
            arr_data_jabatan = tabel_jabatan.row(this).data();
        } else {
            arr_data_jabatan = tabel_jabatan.row($(this).parent().parent().parent().parent().parent().prev()).data();
        }
        if ($(this).attr('class').includes('dtl-jabatan')) {
            $('#modal-edit-jabatan').modal('show');
            $('#ename').val(arr_data_jabatan[1]);
            $('#id-jabatan').val(arr_data_jabatan[3])
        } else {
            swal({
              title: "Apakah Anda Yakin Ingin Menghapus Data "+arr_data_jabatan[1]+"?",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((confirm) => {
              if (confirm) {
                delete_jabatan(arr_data_jabatan[3]);
              }
            });
        }
	});
})