 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PegawaiController extends CI_Controller {

	function __construct(){
		parent::__construct();
		if ($this->session->userdata('session_pegawai')) {
			$this->load->model('PegawaiModel');
        	$this->load->helper(array('form', 'url'));
        	$this->load->library('form_validation');
		} else {
			redirect(site_url('login/LoginController'), 'refresh');
		}
       
	}
	public function view_pegawai_insource()
	{
		$this->load->view('header');
		$this->load->view('pegawai/index.php');
		$this->load->view('pegawai/modal_tambah_pegawai.php');
		$this->load->view('footer');
	}

	public function grafik_pegawai()
	{
		$this->load->view('header');
		$this->load->view('pegawai/grafik_pegawai.php');
		$this->load->view('footer');
	}

	public function list_pegawai()
	{

		$data1 = $this->PegawaiModel->GetDataPegawai();
		$data = array("data"=>$data1);
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}

	public function view_pegawai_outsource()
	{
		$this->load->view('header');
		$this->load->view('pegawai/index.php');
		$this->load->view('pegawai/modal_tambah_pegawai.php');
		$this->load->view('footer');
	}

	public function list_pegawai_outsource()
	{
		$data1 = $this->PegawaiModel->GetDataPegawaiOutsource();
		$data = array("data"=>$data1);
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}

	public function save_pegawai()
	{
		$user = $this->session->userdata('session_pegawai');

		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_message('matches', '%s Tidak Sesuai');
		$this->form_validation->set_rules('nama_pegawai', 'Nama Pegawai', 'required');
		$this->form_validation->set_rules('nik', 'NIK', 'required');
		$this->form_validation->set_rules('no_ktp', 'No KTP', 'required');
		$this->form_validation->set_rules('role', 'Role Pegawai', 'required');
		$this->form_validation->set_rules('employee_status', 'Status Pegawai', 'required');

		if ($this->input->post('role')=='1' && empty($this->input->post('id_pegawai'))) {
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('password2', 'Confirm Password', 'required|matches[password]');

		}
		
		if ($this->form_validation->run()!=FALSE) {
			$path_foto = $this->config->item('path_upload'); 
			$foto = $this->input->post('nama-foto');
			$password = md5($this->input->post('password'));
    		$data = array('nama_pegawai' => $this->input->post('nama_pegawai'),
				'id' => $this->input->post('id_pegawai'), 
				'nik' => $this->input->post('nik'),
				'no_ktp' => $this->input->post('no_ktp'),
				'alamat' => $this->input->post('alamat'),
				'tempat_lahir' => $this->input->post('tempat_lahir'),
				'tanggal_lahir' => date('Y-m-d',strtotime($this->input->post('tanggal_lahir'))),
				'no_hp' => $this->input->post('no_hp'),
				'foto' =>$foto,
				'jenis_kelamin' => $this->input->post('jenis_kelamin'),
				'role' => $this->input->post('role'),
				'username' => $this->input->post('username'),
				'password' => $password,
				'employee_status' => $this->input->post('employee_status'),
				'created_by' => $user->employee_id,
				'updated_by' => $user->employee_id
				);  
				
		    $result=$this->PegawaiModel->save_data_pegawai($data);
	        $this->output->set_output($result);
		} else {
			 $this->output->set_output(validation_errors());
		}
		
	}

	public function upload_foto()
	{
		$valid_extensions = array('jpeg','jpg','png'); // valid extensions
		$path =$this->config->item('path_upload'); // upload directory
		$full_path =$this->config->item('base_url').$path;
		
		$final_image = '';
		$msg='';
		// $_FILES['foto'];
		if(isset($_FILES['foto']))
		{
			$img = $_FILES['foto']['name'];
			$tmp = $_FILES['foto']['tmp_name'];
				
			// get uploaded file's extension
			$ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
			
			// can upload same image using rand function
			$final_image = rand(1000,1000000).$img;
			
			// check's valid format
			if(in_array($ext, $valid_extensions)) 
			{	
				//$msg = 'tipe ada';				
				$path = $path.strtolower($final_image);	
				
				if(move_uploaded_file($tmp,$path)) 
				{
					$msg = $path;
					//echo "<img src='$full_path$final_image' width='100px' height='100px'/>";
				} else {
					$msg = 'Gagal Upload, Ukuran Maksimal 1 Mb';
				}
			} 
			else 
			{	$msg = 'Gagal Upload, Format Tidak Sesuai';
				echo 'invalid';
			}
			
			$this->output->set_output(trim($msg));
		}
	}

	public function get_pegawai_by_id()
	{
		$id = (int)$this->input->post('id');
		$result = $this->PegawaiModel->get_pegawai_by_id($id);
		$this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($result));
	}

	public function delete_pegawai()
	{
		$id=(int)$this->input->post('id');
		$result = $this->PegawaiModel->delete_pegawai($id);
		$this->output->set_content_type('application/text');
        $this->output->set_output($result);
	}

	/*public function upload_image()
	{
		$status = "";
	    $msg = "";
	    $file_element_name = 'foto';//$this->input->post('foto_'); 

    	if (!empty($file_element_name)) {

		    if ($status != "error")
		    {
		        $config['upload_path'] = './assets/upload/';
		        $config['allowed_types'] = 'gif|jpg|png|doc|txt';
		        $config['max_size'] = 1024 * 8;
		        $config['encrypt_name'] = TRUE;
		 
		        $this->load->library('upload', $config);
		 		
		        if (!$this->upload->do_upload($file_element_name))
		        {
		        	
		            $status = 'error';
		            $msg = $this->upload->display_errors('', '');
		        }
		        else
		        {

		            $data = $this->upload->data();
		            return $data['file_name'];
		           	
		            if($file_id)
		            {
		                $status = "success";
		                $msg = "File successfully uploaded";
		            }
		            else
		            {
		                unlink($data['full_path']);
		                $status = "error";
		                $msg = "Something went wrong when saving the file, please try again.";
		            }
		        }
		        @unlink($_FILES[$file_element_name]);
		    }	
		}
	    echo json_encode(array('status' => $status, 'msg' => $msg));
	}*/

	public function update_password(){
		$this->form_validation->set_message('matches', '%s Tidak Sesuai');
		$this->form_validation->set_message('required', '%s Harus Diisi');
		$this->form_validation->set_rules('u_password','Password','required');
		$this->form_validation->set_rules('u_password2', 'Confirm Password', 'required|matches[u_password]');

		if ($this->form_validation->run()!=FALSE) {
			$nik = $this->input->post('u_nik');
			$password = md5($this->input->post('u_password'));
			$result=$this->PegawaiModel->update_password($nik,$password);
			$this->output->set_output($result);
		} else {
			$this->output->set_output(validation_errors());
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */