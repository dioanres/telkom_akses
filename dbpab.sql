-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 09, 2018 at 05:40 PM
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbpab`
--
CREATE DATABASE IF NOT EXISTS `dbpab` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `dbpab`;

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
CREATE TABLE IF NOT EXISTS `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT NULL,
  `fullname` varchar(50) NOT NULL,
  `password` varchar(50) DEFAULT NULL,
  `gender` char(1) NOT NULL,
  `address` text,
  `no_handphone` varchar(15) NOT NULL,
  `no_handphone2` varchar(15) DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `role_id` int(11) NOT NULL COMMENT '1: Admin, 2: User',
  `employee_id` varchar(45) NOT NULL,
  `birth_place` varchar(100) DEFAULT NULL,
  `id_card_no` varchar(20) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `employee_status` varchar(3) NOT NULL COMMENT 'out = outsource, in = insource',
  `birth_date` date DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL COMMENT '1=Aktif, 0 = Non Aktif',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `username`, `fullname`, `password`, `gender`, `address`, `no_handphone`, `no_handphone2`, `image`, `role_id`, `employee_id`, `birth_place`, `id_card_no`, `created_date`, `created_by`, `updated_date`, `updated_by`, `employee_status`, `birth_date`, `is_active`) VALUES
(1, 'ABC', 'ANDI', 'admin', '', 'ALAMAT', '0823880980', '', 'C:fakepathIMG_20170916_100705_034.jpg', 1, '10988080', 'PADANG', '090890890809', NULL, NULL, NULL, NULL, 'in', '0000-00-00', 0),
(2, '', 'asst ciio', 'admin', '', 'dddddddddd', '', '', 'assets/upload/859211forgotpageleft1.png', 1, '22321', 'tempat', '1', NULL, NULL, NULL, NULL, 'out', '0000-00-00', 1),
(3, 'manager', 'manager', 'admin', '', 'adsasd', '', '', 'C:fakepath19-1User-512.png', 1, '4576', '', '', NULL, NULL, NULL, NULL, 'in', '0000-00-00', 0),
(4, 'spv', 'spv', 'admin', 'L', 'adsasd', '', '', '', 1, '3245', NULL, '332343', NULL, NULL, NULL, NULL, 'out', NULL, 0),
(5, 'dio anressssss', 'dio andri restu', '123', '', 'alamat', '0823', NULL, 'assets/upload/naruto.jpg', 1, '87687687', 'padang', '14090909', NULL, NULL, NULL, NULL, 'in', '0000-00-00', 0),
(6, 'hlhlk', 'nama pegawai', 'hklhklh', '', 'lkjkl', 'jkljklj', '', 'C:fakepath413842.jpg', 2, 'kjlk', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(8, 'op', 'jl', 'hklhklh', 'L', 'lkjkl', 'jkljklj', '', 'C:fakepathinus.PNG', 2, 'kjlk', 'jlk', 'jkljj', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(9, '', 'yiu', '', 'L', 'uyiu', 'uiyiu', '', 'C:fakepathIMG-20180122-WA0001.jpg', 1, 'yiuy', 'yiu', 'iuy', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'out', '0000-00-00', 0),
(14, 'qwe', 'pegawai outsource', '', 'L', 'ljlk', '8098098', '', '', 2, '80980', '8908', '809809', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'out', '1989-08-09', 0),
(51, '', 'qwerty', '', 'L', 'alamat', '', '', 'assets/upload/CUTI13.PNG', 2, '123234', '', '1111111111', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(52, '', 'akliuiui', '', 'L', 'jklj', '8980', '', 'assets/upload/Capture.PNG', 2, 'jjl', 'j', 'j', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(53, '', 'aaaaaaaaaaa', '', 'L', 'alasa', '', '', 'assets/upload/binus1.PNG', 2, 'qqqqqq', '', '11111111111111111', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(54, '', 'aaaaaaaaaaa', '', 'L', 'alasa', '', '', 'assets/upload/binus2.PNG', 2, 'qqqqqq', '', '11111111111111111', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(55, 'qwq', 'andri', 'wqwq', 'L', '111', '0812132', '', 'assets/upload/IMG-20180122-WA0001.jpg', 2, '11111', '111', '11111', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '2018-02-14', 0),
(56, 'asst', 'asst ciio', 'admin', '', 'dddddddddd', '', '', 'assets/upload/ssh_keygen.PNG', 1, '22321', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'out', '0000-00-00', 0),
(57, '', 'nama dio ', '', '', 'jklj', '8980', '', '', 2, '12345', '', '12345', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(58, '', 'nama dio', '', '', 'jklj', '8980', '', '', 2, '12345', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(59, '', 'nama dio', '', '', 'jklj', '8980', '', '', 2, '12345', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(60, '', 'nama dio', '', '', 'jklj', '8980', '', '', 2, '12345', '', '9999999999999999999', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(61, '', 'nama dio', '', '', 'jklj', '8980', '', '', 2, '12345', '', '9999999999999999999', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(62, '', 'nama dio', '', '', 'jklj', '8980', '', '', 2, '12345', '', '9999999999999999999', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(63, '', 'nama dio', '', '', 'jklj', '8980', '', '', 2, '12345', '', '9999999999999999999', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(64, '', 'qqqqqqqqqq', '', 'L', 'q', '', '', '', 2, 'qqqqqqq', 'qqq', 'qqqq', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'out', '0000-00-00', 0),
(65, '', 'qqqqqqqqqq', '', 'L', 'q', '', '', 'assets/upload/11.PNG', 2, 'qqqqqqq', 'qqq', 'qqqq', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'out', '0000-00-00', 0),
(66, '', 'Fixing TAmbah Foto', '', 'L', 'alamat lengkap', '0983848848484', '', '', 2, '778888188', 'Jakarta', '126336484678', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '1994-06-17', 0),
(67, '', 'Fixing TAmbah Foto', '', '', 'alamat lengkap', '0983848848484', '', 'assets/upload/19-1User-5122.png', 2, '778888188', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(68, 'eq', 'qeeqe', '', 'L', 'eqe', '', '', '', 2, 'eqe', 'eqeq', 'eqe', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '2018-02-15', 0),
(69, 'eq', 'qeeqe', '', 'L', 'eqe', '', '', 'assets/upload/CUTI.PNG', 2, 'eqe', 'eqeq', 'eqe', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '2018-02-15', 0),
(70, '', 'lagio fix', '', 'P', 'alamat', '098908908', '', '', 2, '99999999', 'tempat', '08890808', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '2016-11-30', 0),
(71, '', 'lagio fix', '', '', 'alamat', '098908908', '', '', 2, '99999999', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(72, '', 'lagio fix', '', '', 'alamat', '098908908', '', '', 2, '99999999', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(73, '', 'lagio fix', '', '', 'alamat', '098908908', '', '', 2, '99999999', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(74, '', 'lagio fix', '', '', 'alamat', '098908908', '', '', 2, '99999999', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(75, '', 'lagio fix', '', '', 'alamat', '098908908', '', 'assets/upload/240_F_65249991_j1ibKqGbDiOCRGe6jO0Y8', 2, '99999999', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(76, '', 'lagio fix', '', '', 'alamat', '098908908', '', '', 2, '99999999', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(77, '', 'lagio fix', '', '', 'alamat', '098908908', '', '', 2, '99999999', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(78, '', 'lagio fix', '', '', 'alamat', '098908908', '', '', 2, '99999999', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(79, '', 'lagio fix', '', '', 'alamat', '098908908', '', '', 2, '99999999', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(80, '', 'lagio fix', '', '', 'alamat', '098908908', '', '', 2, '99999999', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(81, '', 'lagio fix', '', '', 'alamat', '098908908', '', 'assets/upload/931069240_F_65247527_AG3LhsAVEEj9Mtl', 2, '99999999', '', '5555555', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(82, '', 'lagio fix', '', '', 'alamat', '098908908', '', '', 2, '99999999', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(83, '', 'lagio fix', '', '', 'alamat', '098908908', '', '', 2, '99999999', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(84, '', 'lagio fix', '', '', 'alamat', '098908908', '', '', 2, '99999999', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(85, '', 'lagio fix', '', '', 'alamat', '098908908', '', '', 2, '99999999', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(86, '', 'lagio fix', '', '', 'alamat', '098908908', '', '', 2, '99999999', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(87, '', 'lagio fix', '', '', 'alamat', '098908908', '', '', 2, '99999999', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(88, '', 'lagio fix', '', '', 'alamat', '098908908', '', '', 2, '99999999', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(89, '', 'lagio fix', '', '', 'alamat', '098908908', '', '', 2, '99999999', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(90, '', 'lagio fix', '', '', 'alamat', '098908908', '', '', 2, '99999999', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(91, '', 'ioi', '', 'L', 'iopi', '', '', '', 2, 'oiopi', 'opipo', 'poiop', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(92, '', 'ssssssssssss', '', 'L', 'sssssssssssss', '', '', '', 2, 'ss333', 'sssssssssssssss', '333333333333', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(93, '', 'ddddddddd', '', 'L', 'ddddddddddddd', '4444444444', '', '53013319-1User-512.png', 2, 'ddddddddddddd', '', 'ddddddddddddd', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(94, '', 'aaaaaaaaaaaaaaaaa', '', 'L', 'a', 'a', '', '79948719-1User-512.png', 2, 'aaaaaaaaaa', 'a', 'aaaaaa', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(95, '', 'ggggggggggg', '', 'L', 'g', '', '', '305840240_F_65247527_AG3LhsAVEEj9Mtl43m7akdYz6KJ0j', 2, 'g', 'g', 'g', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 0),
(96, '', 'ridho', '', '', 'r', '4444444444', '', 'assets/upload/374040413842.jpg', 2, 'rrrrrrrrr', 'padang', 's', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 1),
(97, '', 'andri', '', '', 'alamat satu', '0898', '', 'assets/upload/707843413842.jpg', 2, '098908', '', '222', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 1),
(98, '', 'namaaaa', '', 'L', '898', '890', '', 'assets/upload/99634219-1User-512.png', 2, '089', '898', '890', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 1),
(99, '', 'arif', '', 'L', 'alamat', '32323232', '', 'assets/upload/740342413842.jpg', 2, '098989', '', '090990', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 1),
(100, '', 'ds', '', 'L', '', '333', '', 'assets/upload/974391bjhz6wgbszz.jpg', 2, 'dsdsd', '', '333333', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 1),
(101, '', 'dio andri', '21232f297a57a5a743894a0e4a801fc3', 'L', 'alamat', '212121', '', 'assets/upload/747964413842.jpg', 1, '4321', 'padang', '999990890', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'in', '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `employee_organizational`
--

DROP TABLE IF EXISTS `employee_organizational`;
CREATE TABLE IF NOT EXISTS `employee_organizational` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_id` (`employee_id`),
  KEY `position_id` (`position_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_organizational`
--

INSERT INTO `employee_organizational` (`id`, `parent_id`, `employee_id`, `position_id`) VALUES
(1, 0, 1, 1),
(2, 1, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `employee_performance`
--

DROP TABLE IF EXISTS `employee_performance`;
CREATE TABLE IF NOT EXISTS `employee_performance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `no_sc` varchar(45) NOT NULL,
  `no_telpon_pelanggan` varchar(45) NOT NULL,
  `nama_pelanggan` varchar(45) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_performance`
--

INSERT INTO `employee_performance` (`id`, `employee_id`, `no_sc`, `no_telpon_pelanggan`, `nama_pelanggan`, `created_date`, `created_by`, `updated_date`, `updated_by`) VALUES
(1, 1, '0000-00-00', '123', '123', '2018-02-09 15:41:53', NULL, NULL, NULL),
(2, 5, '0000-00-00', '123132', '123asd', '2018-02-09 15:42:13', NULL, NULL, NULL),
(3, 1, '0', '123', '123', '2018-02-09 15:45:05', NULL, NULL, NULL),
(4, 14, '0', '123', '123', '2018-02-09 15:45:18', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

DROP TABLE IF EXISTS `position`;
CREATE TABLE IF NOT EXISTS `position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `position`
--

INSERT INTO `position` (`id`, `name`) VALUES
(1, 'CEO'),
(2, 'ASST. CEO'),
(3, 'MANAGER'),
(4, 'SUPERVISOR');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
