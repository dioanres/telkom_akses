<?php 
   class StrukturPegawaiModel extends CI_Model {
      function __construct() { 
         parent::__construct(); 
      } 
   
      public function get_struktur() { 
         $sql = 'SELECT eo.id "Id", eo.parent_id "ParentId", UPPER(e.fullname) "Name", UPPER(p.name) "Title", e.image "Image"
                  FROM employee_organizational eo
                  LEFT JOIN employee e on e.id = eo.employee_id
                  LEFT JOIN position p on p.id = eo.position_id';
         $query = $this->db->query($sql);
         $result = $query->result(); 
         return $result;
      }

      public function cek_struktur($id){
         $this->db->where('position_id', $id);
         $num_rows = $this->db->count_all_results('employee_organizational');
         return $num_rows;
      }

      public function detail($id) { 
         $sql = 'SELECT eo.*, UPPER(e.fullname) "Name", UPPER(p.name) "Title", e.image "Image",  "Phone"
                  FROM employee_organizational eo
                  LEFT JOIN employee e on e.id = eo.employee_id
                  LEFT JOIN position p on p.id = eo.position_id
                  WHERE eo.id = ?';
         $query = $this->db->query($sql, array($id));
         $result = $query->row(); 
         return $result;
      }

      public function add_struktur($data){
         $this->db->insert('employee_organizational', $data);
      }

      public function delete_struktur($id){
         $this->db->where('id', $id);
         $this->db->delete('employee_organizational'); 
      }  

      public function edit_struktur($id, $data){
         $this->db->where('id', $id);
         $this->db->update('employee_organizational', $data); 
      }
   }