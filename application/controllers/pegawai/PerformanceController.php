<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PerformanceController extends CI_Controller {

	function __construct(){
		parent::__construct();
        $this->load->model('PerformanceModel');
        $this->load->model('PegawaiModel');
        $this->load->model('PositionModel');
        $this->load->library('form_validation');
	}

	public function index()
	{
		$data['employee'] = $this->PegawaiModel->get_pegawai();
		$this->load->view('header');
		$this->load->view('performance/input_data.php', $data);
		$this->load->view('footer');
	}

	public function get_performance(){
		$data = $this->PerformanceModel->get_performance();
		$this->output->set_output(json_encode($data));
	}

	public function performance_day(){
		$date = $this->input->post('date');
		$data = $this->PerformanceModel->performance_day($date);
		$this->output->set_output(json_encode($data));
	}

	public function performance_day2(){
		$date = $this->input->post('date');
		$date_to = $this->input->post('to');
		$data = $this->PerformanceModel->performance_day($date, $date_to);
		$return['label'] = '';
		$return['value'] = '';
		foreach ($data as $key => $value) {
			$return['label'][] = $value->label;
			$return['value'][] = $value->value;
		}
		$this->output ->set_content_type('application/json')->set_output(json_encode($return));
	}

	public function performance_daily(){
		$date = $this->input->post('date');
		$data = $this->PerformanceModel->performance_daily($date);
		$return['label'] = '';
		$return['value'] = '';
		foreach ($data as $key => $value) {
			$return['label'][] = $value->label;
			$return['value'][] = $value->value;
		}
		$this->output ->set_content_type('application/json')->set_output(json_encode($return));
	}

	public function performance_month(){
		$date = $this->input->post('date');
		$data = $this->PerformanceModel->performance_month($date);
		$return['label'] = '';
		$return['value'] = '';
		foreach ($data as $key => $value) {
			$return['label'][] = $value->label;
			$return['value'][] = $value->value;
		}
		$this->output ->set_content_type('application/json')->set_output(json_encode($return));
	}

	public function performance_year(){
		$date = $this->input->post('date');
		$data = $this->PerformanceModel->performance_year($date);
		$return['label'] = '';
		$return['value'] = '';
		foreach ($data as $key => $value) {
			$return['label'][] = $value->label;
			$return['value'][] = $value->value;
		}
		$this->output ->set_content_type('application/json')->set_output(json_encode($return));
	}

	public function add_performance(){
		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_rules('nama', 'Nama Pelanggan', 'required');
		$this->form_validation->set_rules('employee', 'Pegawai', 'required');
		$this->form_validation->set_rules('sc', 'No SC', 'required');
		$this->form_validation->set_rules('tlp', 'No Telepon Pelanggan', 'required');
		if ($this->form_validation->run() != FALSE){
            $data = array(
				'employee_id' => $this->input->post('employee'),
				'no_sc' => $this->input->post('nsc'),
				'no_telpon_pelanggan' => $this->input->post('tlp'),
				'nama_pelanggan' => $this->input->post('nama'),
				'created_date' => date("Y-m-d H:i:s")
			);

			$this->PerformanceModel->add_performance($data);
			$this->output->set_output('berhasil');
        }else{
        	$this->output->set_output(validation_errors());
        }
        
	}

	public function edit_performance(){
		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_rules('nama', 'Nama Pelanggan', 'required');
		$this->form_validation->set_rules('employee', 'Pegawai', 'required');
		$this->form_validation->set_rules('sc', 'No SC', 'required');
		$this->form_validation->set_rules('tlp', 'No Telepon Pelanggan', 'required');
		if ($this->form_validation->run() != FALSE){
            $data = array(
				'employee_id' => $this->input->post('employee'),
				'no_sc' => $this->input->post('nsc'),
				'no_telpon_pelanggan' => $this->input->post('tlp'),
				'nama_pelanggan' => $this->input->post('nama'),
				'created_date' => date("Y-m-d H:i:s")
			);

			$this->PerformanceModel->edit_performance($this->input->post('idperformance'), $data);
			$this->output->set_output('berhasil');
        }else{
        	$this->output->set_output(validation_errors());
        }
        
	}

	public function delete_performance(){
		$id = $this->input->post('id');
		$this->PerformanceModel->delete_performance($id);
		$this->output->set_output('berhasil');
	}

	public function detail(){
		$id = $this->input->post('id');
		$data = $this->PerformanceModel->detail($id);
		$this->output->set_output(json_encode($data));
	}

	function _notMatch($value1, $value2){
	   if($value1 == $this->input->post($value2)){
	       $this->form_validation->set_message('_notMatch', 'Pegawai dan Atasan harus berbeda');
	       return false;
	   }
	   return true;
	}
	
}