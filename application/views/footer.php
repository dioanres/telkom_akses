<!-- jQuery -->

    <script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/jquery-2.1.4.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/bootstrap-datepicker.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>assets/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="<?php echo base_url(); ?>assets/vendor/raphael/raphael.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/morrisjs/morris.min.js"></script>
    <!-- <script src="<?php echo base_url(); ?>assets/data/morris-data.js"></script> -->

    <!-- Custom Theme JavaScript -->
   

    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url(); ?>assets/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/datatables-plugins/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/datatables-plugins/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/datatables-responsive/dataTables.responsive.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/datatables-plugins/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/datatables-plugins/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/datatables-plugins/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/datatables-plugins/buttons.html5.min.js"></script>
   
    <script src="<?php echo base_url(); ?>assets/vendor/toastr/toastr.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/dist/js/sb-admin-2.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/chartjs/chartjs-plugin-datalabels.js"></script>
    
    <script>
        var CSS_COLOR_NAMES = ["AliceBlue","AntiqueWhite","Aqua","Aquamarine","Azure","Beige","Bisque","Black","BlanchedAlmond","Blue","BlueViolet","Brown","BurlyWood","CadetBlue","Chartreuse","Chocolate","Coral","CornflowerBlue","Cornsilk","Crimson","Cyan","DarkBlue","DarkCyan","DarkGoldenRod","DarkGray","DarkGrey","DarkGreen","DarkKhaki","DarkMagenta","DarkOliveGreen","Darkorange","DarkOrchid","DarkRed","DarkSalmon","DarkSeaGreen","DarkSlateBlue","DarkSlateGray","DarkSlateGrey","DarkTurquoise","DarkViolet","DeepPink","DeepSkyBlue","DimGray","DimGrey","DodgerBlue","FireBrick","FloralWhite","ForestGreen","Fuchsia","Gainsboro","GhostWhite","Gold","GoldenRod","Gray","Grey","Green","GreenYellow","HoneyDew","HotPink","IndianRed","Indigo","Ivory","Khaki","Lavender","LavenderBlush","LawnGreen","LemonChiffon","LightBlue","LightCoral","LightCyan","LightGoldenRodYellow","LightGray","LightGrey","LightGreen","LightPink","LightSalmon","LightSeaGreen","LightSkyBlue","LightSlateGray","LightSlateGrey","LightSteelBlue","LightYellow","Lime","LimeGreen","Linen","Magenta","Maroon","MediumAquaMarine","MediumBlue","MediumOrchid","MediumPurple","MediumSeaGreen","MediumSlateBlue","MediumSpringGreen","MediumTurquoise","MediumVioletRed","MidnightBlue","MintCream","MistyRose","Moccasin","NavajoWhite","Navy","OldLace","Olive","OliveDrab","Orange","OrangeRed","Orchid","PaleGoldenRod","PaleGreen","PaleTurquoise","PaleVioletRed","PapayaWhip","PeachPuff","Peru","Pink","Plum","PowderBlue","Purple","Red","RosyBrown","RoyalBlue","SaddleBrown","Salmon","SandyBrown","SeaGreen","SeaShell","Sienna","Silver","SkyBlue","SlateBlue","SlateGray","SlateGrey","Snow","SpringGreen","SteelBlue","Tan","Teal","Thistle","Tomato","Turquoise","Violet","Wheat","White","WhiteSmoke","Yellow","YellowGreen"];
    var base_url = "<?=base_url()?>"
    var controller = "<?=$this->uri->segment(2);?>"
    $('.datepicker').datepicker({
        defaultDate: new Date(),
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd'
    });
    $('.datepickerbulan').datepicker({
        defaultDate: new Date(),
        autoclose: true,
        todayHighlight: true,
        format: "yyyy-mm",
        viewMode: "months", 
        minViewMode: "months"
    });
    $('.datepickertahun').datepicker({
        defaultDate: new Date(),
        autoclose: true,
        todayHighlight: true,
        format: "yyyy",
        viewMode: "years", 
        minViewMode: "years"
    });
    $('.datepicker').val("<?=date('Y-m-d')?>")
    $('.datepickerbulan').val("<?=date('Y-m')?>")
    $('.datepickertahun').val("<?=date('Y')?>")
    $('.date-picker,#tanggal-lahir').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'
    });
                //show datepicker when clicking on the icon
              /*  .next().on(ace.click_event, function(){
                    $(this).prev().focus();
                });*/
            
                //or change it into a date range picker
               
    </script>
    <script src="<?php echo base_url(); ?>assets/js/struktur_organisasi.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/data_pegawai.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/performance.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/position.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/report.js"></script>