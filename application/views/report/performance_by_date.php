<!DOCTYPE html>
<html lang="en">
<body>

    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Laporan Kinerja</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-3">
                        <h4>Laporan Kinerja Berdasarkan Tanggal </h4>
                    </div>
                    <div class="col-lg-3">
                        <div class="input-group date" >
                                 <input type="text" class="form-control datepicker" name="report-harian-from" id="report-harian-from">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>   
                    </div>
                    <div class="col-lg-1">
                        <center><h5>Sampai</h5></center>
                    </div>
                    <div class="col-lg-3">
                        <div class="input-group date">
                                 <input type="text" class="form-control datepicker" name="report-harian-end" id="report-harian-end" >
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                    </div>
                    <button type="button" class="btn btn-primary" id="btn-view-report">View Data</button>   
                </div>
                <div>
                    <br></br>
                </div>
                <div class="col-lg-12" hidden="hidden" id="list-report" style="margin-top: 56px">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4>List Data</h4>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped" id="tabel-report-performance-day">
                                <thead>
                                    <tr align="center">
                                        <th>No</th>
                                        <th>NIK</th>
                                        <th>Nama</th>
                                        <th>No HP</th>
                                        <th>No. SC</th>
                                        <th>Nama Pelanggan</th>
                                        <th>No. Telpon Pelanggan</th>
                                        <th>Tanggal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                         <div class="panel-footer">
                            <button type="button" class="btn btn-primary" id="btn-view-report">Close</button>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-8 -->
                
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    

</body>

</html>
