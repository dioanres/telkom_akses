<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class JabatanController extends CI_Controller {

	function __construct(){
		parent::__construct();
        $this->load->model('PositionModel');
        $this->load->model('StrukturPegawaiModel');
        $this->load->library('form_validation');
	}

	public function index()
	{
		$this->load->view('header');
		$this->load->view('pegawai/jabatan_pegawai.php');
		$this->load->view('footer');
	}

	public function get_jabatan(){
		$data = $this->PositionModel->get_position();
		$this->output->set_output(json_encode($data));
	}

	public function add_jabatan(){
		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_rules('name', 'Nama Jabatan', 'required');
		if ($this->form_validation->run() != FALSE){
            $data = array(
				'name' => $this->input->post('name')
			);

			$this->PositionModel->add_jabatan($data);
			$this->output->set_output('berhasil');
        }else{
        	$this->output->set_output(validation_errors());
        }
        
	}

	public function edit_jabatan(){
		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_rules('name', 'Nama Jabatan', 'required');
		if ($this->form_validation->run() != FALSE){
            $data = array(
				'name' => $this->input->post('name')
			);
			$this->PositionModel->edit_jabatan($this->input->post('idjabatan'), $data);
			$this->output->set_output('berhasil');
        }else{
        	$this->output->set_output(validation_errors());
        }
        
	}

	public function delete_jabatan(){
		$id = $this->input->post('id');
		$org = $this->StrukturPegawaiModel->cek_struktur($id);
		if($org > 0){
			$this->output->set_output('Jabatan masih ada dalam organisasi');	
		}else {
			$this->PositionModel->delete_jabatan($id);
			$this->output->set_output('berhasil');
		}
	}

	public function detail(){
		$id = $this->input->post('id');
		$data = $this->PositionModel->detail($id);
		$this->output->set_output(json_encode($data));
	}
}