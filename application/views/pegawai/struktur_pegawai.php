<style type="text/css">
    .get-oc-c{
        background: white;
        overflow: auto !important;
    }

    .get-oc-tb{
        background: white !important;
    }

    .get-org-chart rect.get-box {
        fill: #ffffff;
        stroke: #D9D9D9;
    }

    .get-org-chart .get-text.get-text-0 {
        fill: #262626;
    }
    
    .get-org-chart .get-text.get-text-1 {
        fill: #262626;
    }
    
    .get-org-chart .get-text.get-text-2 {
        fill: #788687;
    }

    .get-green.get-org-chart {
        background-color: #f2f2f2;
    }
    .more-info {
        fill: #18879B;
    }

    .btns path {
        fill: #F8F8F8;
        stroke: #D9D9D9;
    }

    .btns {
        cursor: pointer;
    }
    
    .btns circle {
        fill: #555555;
    }

    .btns line {                
        stroke-width: 3px;
        stroke: #ffffff;
    }         
</style>
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Struktur Organisasi</h1>
            </div>
            <div>
                   
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-sm-12">
                <div id="people"></div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
<!-- Modal -->
<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Tambah Karyawan</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-lg-12">
                        <div class="col-lg-3">
                            <label>Nama Karyawan</label>
                        </div>
                        <div class="col-lg-9">
                            <select class="form-control" id="employee">
                                <option value="">Silakan Pilih</option>
                                <?php
                                foreach ($employee as $key => $value) {
                                    echo "<option value=".$value->id.">".$value->fullname."</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-lg-12 hidden">
                        <div class="col-lg-3">
                            <label>Atasan</label>
                        </div>
                        <div class="col-lg-9">
                            <input id="parent">
                        </div>
                    </div>
                    <div class="form-group col-lg-12">
                        <div class="col-lg-3">
                            <label>Jabatan</label>
                        </div>
                        <div class="col-lg-9">
                            <select class="form-control" id="position">
                                <option value="">Silakan Pilih</option>
                                <?php
                                foreach ($position as $key => $value) {
                                    echo "<option value=".$value->id.">".$value->name."</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="save">Save</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Tambah Karyawan</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-lg-12">
                        <div class="col-lg-3">
                            <label>Nama Karyawan</label>
                        </div>
                        <div class="col-lg-9">
                            <select class="form-control" id="eemployee">
                                <option value="">Silakan Pilih</option>
                                <?php
                                foreach ($employee as $key => $value) {
                                    echo "<option value=".$value->id.">".$value->fullname."</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-lg-12 hidden">
                        <div class="col-lg-3">
                            <label>Atasan</label>
                        </div>
                        <div class="col-lg-9">
                            <!-- <input id="parent"> -->
                        </div>
                    </div>
                    <div class="form-group col-lg-12">
                        <div class="col-lg-3">
                            <label>Jabatan</label>
                        </div>
                        <div class="col-lg-9">
                            <select class="form-control" id="eposition">
                                <option value="">Silakan Pilih</option>
                                <?php
                                foreach ($position as $key => $value) {
                                    echo "<option value=".$value->id.">".$value->name."</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="eid" id="eid">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="esave">Save</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modalNew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Top Level</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-lg-12">
                        <div class="col-lg-3">
                            <label>Nama</label>
                        </div>
                        <div class="col-lg-9">
                            <select class="form-control" id="nemployee">
                                <option value="">Silakan Pilih</option>
                                <?php
                                foreach ($employee as $key => $value) {
                                    echo "<option value=".$value->id.">".$value->fullname."</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-lg-12">
                        <div class="col-lg-3">
                            <label>Jabatan</label>
                        </div>
                        <div class="col-lg-9">
                            <select class="form-control" id="nposition">
                                <option value="">Silakan Pilih</option>
                                <?php
                                foreach ($position as $key => $value) {
                                    echo "<option value=".$value->id.">".$value->name."</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="nsave">Save</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->