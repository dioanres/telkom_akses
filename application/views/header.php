<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Aplikasi PAB</title>
    <!-- Data Tables -->
    <link href="<?php echo base_url(); ?>assets/vendor/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/vendor/datatables-plugins/buttons.dataTables.min.css" rel="stylesheet">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url(); ?>assets/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>assets/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<?php echo base_url(); ?>assets/vendor/morrisjs/morris.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_url(); ?>assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <script src="<?php echo base_url(); ?>assets/vendor/getorgchart/getorgchart.js"></script>
    <link href="<?php echo base_url(); ?>assets/vendor/getorgchart/getorgchart.css" rel="stylesheet" />

    <link href="<?php echo base_url(); ?>assets/vendor/toastr/toastr.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/bootstrap/css/bootstrap-datepicker3.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style type="text/css">
    .dataTables_filter, .pagination {
       float: right;
       text-align: right;
    }

.user-profile img {
    width: 29px;
    height: 29px;
    border-radius: 50%;
    border-top-left-radius: 50%;
    border-top-right-radius: 50%;
    border-bottom-right-radius: 50%;
    border-bottom-left-radius: 50%;
    margin-right: 10px;
}
</style>
</head>
<!-- Navigation -->
    <?php $user = @$this->session->userdata('session_pegawai'); ?>
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.html">APLIKASI KEPEGAWAIAAN PT. PEMUDA ANUGRAH BANGSA</a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <!-- <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i> -->
                    <span class="user-profile"><img src="<?php echo base_url().@$user->image; ?>" alt=""></span>
                    <span>
                        <?php echo @$user->fullname; ?>
                    </span>
                    <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#" data-toggle="modal" data-target="#modal-password"><i class="fa fa-user fa-fw"></i>Update Password</a>
                    </li>
                    <!-- <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                    </li> -->
                    <li class="divider"></li>
                    <li><a href="<?php echo base_url(); ?>login/LoginController/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->
       <!--  <hr> -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search">
                        <div class="input-group custom-search-form" style="">
                            <img src="<?php echo base_url(); ?>assets/img/logo copy.png" style="width: 199px;height: : 135px;margin-bottom: -25px;margin-top: -23px;">
                        </div>
                        <!-- /input-group -->
                    </li>
                    <li>
                        <a href="<?php echo base_url()?>home/HomeController"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url()?>pegawai/JabatanController">
                            <i class="fa fa-dashboard fa-fw"></i> Jabatan
                        </a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-table fa-fw"></i> Data Pegawai<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li id="pegawai-in">
                                <a href="<?php echo base_url()?>pegawai/PegawaiController/view_pegawai_insource">Pegawai Insource</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url()?>pegawai/PegawaiController/view_pegawai_outsource">Pegawai Outsource</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-table fa-fw"></i> Data Performance Pegawai<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li id="">
                                <a href="<?php echo base_url()?>pegawai/PerformanceController/">Input Data</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url()?>report/ReportPerformanceController">Laporan Performance</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo base_url()?>pegawai/StrukturPegawaiController/"><i class="fa fa-sitemap fa-fw"></i> Struktur Organisasi</a>
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>
    <div class="modal fade" id="modal-password" role="dialog">
    <div class="modal-dialog">
        <form method="post" id="form-update-password" >
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Update Password</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-lg-12" id="u_div-nik">
                            <div class="col-lg-4">
                                <label>Nik <span class="text-danger">*</span></label>
                            </div>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" id=u_nik name="u_nik" readonly value="<?php echo @$this->session->userdata('session_pegawai')->employee_id ?>">        
                            </div>
                        </div>
                        <div class="form-group col-lg-12" id="u_div-nama">
                            <div class="col-lg-4">
                                <label>Nama <span class="text-danger">*</span></label>
                            </div>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" id=u_nama name="u_nama" disabled="disabled" value="<?php echo @$this->session->userdata('session_pegawai')->fullname ?>">        
                            </div>
                        </div>
                        <div class="form-group col-lg-12" id="u_div-password">
                            <div class="col-lg-4">
                                <label>Password <span class="text-danger">*</span></label>
                            </div>
                            <div class="col-lg-8">
                                <input type="password" class="form-control" id=u_password name="u_password" placeholder="Password">        
                            </div>
                        </div>
                         <div class="form-group col-lg-12" id="u_div-password2">
                            <div class="col-lg-4">
                                <label>Konfirmasi Password <span class="text-danger">*</span></label>
                            </div>
                            <div class="col-lg-8">
                                <input type="password" class="form-control" id=u_password2 name="u_password2" placeholder="Konfirmasi Password">        
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" id="btn-update-password">Simpan</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
  </div>
