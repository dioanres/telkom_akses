<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ReportPerformanceController extends CI_Controller {

	function __construct(){
		parent::__construct();
        $this->load->model('ReportPerformanceModel');
        $this->load->helper(array('form', 'url'));
	}

	public function index()
	{
		$this->load->view('header');
		$this->load->view('report/performance_by_date.php');
		$this->load->view('footer');
	}

	public function list_report_performance_day($date1 = null)
	{
		$date1 = $this->input->post('date_from') ;
		$date2 = $this->input->post('date_end') ;

		/*var_dump($data1);
		exit();*/
		$data = $this->ReportPerformanceModel->performancebydate($date1,$date2);
		/*var_dump($data1);
		exit();*/
		//$data = array("data"=>$data1);
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}

	public function view_report_pdf()
	{
		$date1 = '2018-02-15'; //$this->input->post('date_from') ;
		$date2 = '2018-02-17';///$this->input->post('date_end') ;
		$data['list'] = $this->ReportPerformanceModel->performancebydate($date1,$date2);
		//$this->load->view('header');
		
		//$this->load->view('footer');

		$this->load->helper('dompdf');
		$html = $this->load->view('report/report_pdf.php',$data,true);
		/*var_dump($html);
		exit;*/
		$filename = 'Laporan Performance';
        $paper = 'LEGAL';
        $orientation = 'landscape';
        pdf_create($html, $filename, $paper, $orientation, 'Laporan_Performance');
	}
	
}