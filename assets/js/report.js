

date_from = $('#report-harian-from').val();
date_end  = $('#report-harian-end').val();
 //$('#tabel-report-performance-day').append('<caption style="caption-side: top">Aplikasi E-Pegawai</caption>');
var tabel_lap_performance_harian = $('#tabel-report-performance-day').DataTable({
	"filter": false,
    "paging": false,
    "info": false,
   dom: 'Bfrtip',
        buttons: [
        {
            extend: 'excel',
            text : 'Export to Excel',
            title: 'Laporan Performance Pegawai per tanggal '+date_from+' s/d ' +date_end,
            filename: function(){
                /*var d = new Date();
                var n = d.getTime();*/
                return 'Laporan_Performance ' + date_from +' s/d '+date_end;
            }
        },
        { 
        	extend: 'pdf',
            text : 'Export to PDF',
            messageTop: '',
            title:'Laporan Performance Pegawai per tanggal '+date_from+' s/d ' +date_end,
            className:'center',
            pageSize: 'LEGAL',
            filename: function(){
                return 'Laporan_Performance ' + date_from +' s/d '+date_end;
            }
        }
        ]
});
console.log(tabel_lap_performance_harian.buttons.messageTop);
$('#btn-view-report').click(function(){
	$('#list-report').show();
	list_report_performance();
	/*date_from = $('#report-harian-from').val();
	date_end  = $('#report-harian-end').val();
	console.log(date_from+" "+date_end);
	var tabel_lap_performance_harian = $('#tabel-report-performance-day').DataTable({
    "ajax":base_url+"report/ReportPerformanceController/list_report_performance_day",
    "dataSrc":"data",
    "data" : {
    	date_from:date_from,
    	date_end:date_end
    },
    "columns": [
        {"data":"nomor"},
        { "data": "employee_id" },
        { "data": "fullname" },
        { "data": "no_handphone" },
        { "data": "no_sc" },
        { "data": "nama_pelanggan"},
        {"data" : "no_telpon_pelanggan"}
        ],
	});*/
	
});

function list_report_performance() {
    tabel_lap_performance_harian.clear().draw()
    $.ajax({
        url: base_url+"report/ReportPerformanceController/list_report_performance_day",
        type: 'post',
        cache: false,
        data : {
        	date_from : $('#report-harian-from').val(),
			date_end  : $('#report-harian-end').val()
        },
        //async: false,
        success: function(response, status, error, pesan) {
            console.log(response);
            //response = $.parseJSON(response);
            console.log(response);
            var i = 1;
            $.each(response, function() {
                tabel_lap_performance_harian.row.add([
                    i,
                    this['employee_id'],
                    this['fullname'],
                    this['no_handphone'],
                    this['no_sc'],
                    this['nama_pelanggan'],
                    this['no_telpon_pelanggan'],
                    this['tanggal']
                ]).draw(false);
                i++;
            });

        },
        error: function(response) {
            console.log(response['responseText']);
        }
    });
}
