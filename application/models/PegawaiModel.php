<?php 
   class PegawaiModel extends CI_Model {
	
      function __construct() { 
         parent::__construct(); 
      }

     public function get_pegawai(){
         $sql = 'select * from employee where is_active=1';
         $query = $this->db->query($sql);
         return $query->result();
      }
   		
   	public function GetDataPegawai() {
      	$sql = "select @no:=@no+1 as nomor,e.* from dbpab.employee e,(SELECT @no:= 0) AS no where employee_status='in' and is_active = 1";

      	$query = $this->db->query($sql);
        $result = $query->result(); 
        return $result;
      } 

     public function GetDataPegawaiOutsource() {
      	$sql = "select @no:=@no+1 as nomor,e.* from dbpab.employee e,(SELECT @no:= 0) AS no where employee_status='out' and is_active = 1";

      	$query = $this->db->query($sql);
        $result = $query->result(); 
        return $result;
      }

    public function save_data_pegawai($data) {
    	
    	$id = $data['id'];
  		$username = $data['username'];
  		$fullname = $data['nama_pegawai'];
  		$password = $data['password'];
  		$jenis_kelamin = $data['jenis_kelamin'];
  		$alamat = $data['alamat'];
  		$no_hp = $data['no_hp'];
  		$foto = $data['foto'];
  		$role = $data['role'];
  		$nik = $data['nik'];
  		$tempat_lahir =$data['tempat_lahir'];
  		$tanggal_lahir =$data['tanggal_lahir'];
  		$no_ktp =$data['no_ktp'];
  		$employee_status=$data['employee_status'];
  		$user = $data['created_by'];
  		$date = date("Y-m-d H:i:s");//date('');

  		if (empty($id)) {
  			$ret = $this->db->query("insert into employee 
  		(username,fullname,password,gender,address,no_handphone,no_handphone2,image,role_id,employee_id,birth_place,birth_date,id_card_no,created_date,created_by,updated_date,updated_by,employee_status,is_active) values ('$username','$fullname','$password','$jenis_kelamin','$alamat','$no_hp','','$foto',$role,'$nik','$tempat_lahir','$tanggal_lahir','$no_ktp',sysdate(),'$user','','','$employee_status',1)");
  		} else {
  			$ret = $this->db->query("update employee set username= '$username',fullname = '$fullname',password='$password',gender='$jenis_kelamin',address='$alamat',no_handphone='$no_hp',image='$foto',role_id='$role',employee_id='$nik',birth_place='$tempat_lahir',birth_date='$tanggal_lahir',id_card_no='$no_ktp',employee_status='$employee_status',updated_by='$user',updated_date=sysdate() where id = $id");
  		}
  		
  		return $ret;
		
    }

    public function get_pegawai_by_id($id) {
    	$result = $this->db->query("select * from employee where id = $id")->row();
    	return $result;
    } 

    public function delete_pegawai($id) {
    	$ret = $this->db->query("update employee set is_active=0 where id=$id");
    	return $ret;
    }

    public function modal_upload_image($nik,$foto){
    	$ret = $this->db->query("insert into employee (employee_id,image) values ('$foto','$nik')");
    	return $ret;
    }

    public function update_password($nik,$password){
      $data = $this->session->userdata('session_pegawai');
      $ret = $this->db->query("update employee set password='$password',updated_by='$data->employee_id',updated_date=sysdate() where employee_id='$nik'");
      return $ret;
    }
} 