<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Performance Karyawan</h1>
            </div>
            <div>
                   
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                       <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-performance">Tambah Data</button> 
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <table width="100%" class="table table-striped" id="tabel-performance">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Pegawai</th>
                                    <th>No SC</th>
                                    <th>Pelanggan</th>
                                    <th>No. Telp. Pelanggan</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                                   
                <!-- /.panel -->
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->


<div class="modal fade" id="modal-performance" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" id="form-performance" action="#" enctype="multipart/form-data">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Form Tambah Data</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-lg-12">
                        <div class="col-lg-3">
                            <label>Pegawai</label>
                        </div>
                        <div class="col-lg-9">
                            <select class="form-control" name="employee" id="nemployee">
                                <option value="">Silakan Pilih</option>
                                <?php
                                foreach ($employee as $key => $value) {
                                    echo "<option value=".$value->id.">".$value->fullname."</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group col-lg-12">
                        <div class="col-lg-3">
                            <label>No. SC</label>
                        </div>
                        <div class="col-lg-9">
                            <input type="text" name="sc" id="nsc" class="form-control">
                        </div>
                    </div>

                    <div class="form-group col-lg-12">
                        <div class="col-lg-3">
                            <label>Nama Pelanggan</label>
                        </div>
                        <div class="col-lg-9">
                            <input type="text" maxlength="45" name="nama" id="nnama" class="form-control">
                        </div>
                    </div>

                    <div class="form-group col-lg-12">
                        <div class="col-lg-3">
                            <label>Telepon Pelanggan</label>
                        </div>
                        <div class="col-lg-9">
                            <input type="text" maxlength="20" name="tlp" id="ntlp" class="form-control">
                        </div>
                    </div>
                    
                </div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="input" class="btn btn-primary" id="save-performance" name="save" >Save</button>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-edit-performance" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" id="form-edit-performance" action="#" enctype="multipart/form-data">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Form Edit Data</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-lg-12">
                        <div class="col-lg-3">
                            <label>Pegawai</label>
                        </div>
                        <div class="col-lg-9">
                            <select class="form-control" name="employee" id="eemployee">
                                <option value="">Silakan Pilih</option>
                                <?php
                                foreach ($employee as $key => $value) {
                                    echo "<option value=".$value->id.">".$value->fullname."</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group col-lg-12">
                        <div class="col-lg-3">
                            <label>No. SC</label>
                        </div>
                        <div class="col-lg-9">
                            <input type="text" name="sc" id="esc" class="form-control">
                        </div>
                    </div>

                    <div class="form-group col-lg-12">
                        <div class="col-lg-3">
                            <label>Nama Pelanggan</label>
                        </div>
                        <div class="col-lg-9">
                            <input type="text" maxlength="45" name="nama" id="enama" class="form-control">
                        </div>
                    </div>

                    <div class="form-group col-lg-12">
                        <div class="col-lg-3">
                            <label>Telepon Pelanggan</label>
                        </div>
                        <div class="col-lg-9">
                            <input type="text" maxlength="20" name="tlp" id="etlp" class="form-control">
                        </div>
                    </div>
                    
                </div>
            </div>
            
            <div class="modal-footer">
                <input type="hidden" name="idperformance" id="id-performance">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="input" class="btn btn-primary" id="edit-performance" name="save" >Save</button>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal