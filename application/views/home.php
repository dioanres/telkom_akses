<!DOCTYPE html>
<html lang="en">
<body>

    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashoard</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading row" style="margin-right: 0px; margin-left: 0px">
                            <div class="col-sm-6" style="padding-top: 8px"><i class="fa fa-bar-chart-o fa-fw"></i> Performance Dalam Setahun</div>
                            <div class="col-sm-6">
                                <div class="pull-right">
                                    <div class="input-group date">
                                        <input type="text" name="tanggal-chart-tahun" id="tanggal-chart-tahun" class="form-control datepickertahun">
                                        <label class="input-group-addon" for="tanggal-chart-tahun">
                                            <span class="glyphicon glyphicon-th"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body" style="min-height: 10px;">
                            <div class="row">
                                <!-- /.col-lg-4 (nested) -->
                                <div class="col-lg-12">
                                    <canvas id="pegawai-tahun-area"></canvas>
                                </div>
                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-default">
                        <div class="panel-heading row" style="margin-right: 0px; margin-left: 0px">
                            <div class="col-sm-6" style="padding-top: 8px"><i class="fa fa-bar-chart-o fa-fw"></i> Performance Karyawan per Periode</div>
                            <div class="col-sm-3">
                                <div class="pull-right">
                                    <div class="input-group date">
                                        <input type="text" name="tanggal-chart-harian" id="tanggal-chart-harian" class="form-control datepicker">
                                        <label class="input-group-addon" for="tanggal-chart-harian">
                                            <span class="glyphicon glyphicon-th"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="pull-right">
                                    <div class="input-group date">
                                        <input type="text" name="tanggal-chart-harian-sampai" id="tanggal-chart-harian-sampai" class="form-control " placeholder="Sampai Tanggal">
                                        <label class="input-group-addon" for="tanggal-chart-harian-sampai">
                                            <span class="glyphicon glyphicon-th"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body" style="min-height: 10px;">
                            <div class="row">
                                <!-- /.col-lg-4 (nested) -->
                                <div class="col-lg-12">
                                    <canvas id="pegawai-hari-bar"></canvas>
                                </div>
                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
                <!-- /.col-lg-8 -->
                <div class="col-lg-6">
                    <!-- /.panel -->
                    <div class="panel panel-default">
                        <div class="panel-heading row" style="margin-right: 0px; margin-left: 0px">
                            <div class="col-sm-12" style="padding-top: 8px"><i class="fa fa-bar-chart-o fa-fw"></i> Performance Karyawan per Periode</div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body" style="min-height: 10px;">
                            <div class="row">
                                <!-- /.col-lg-4 (nested) -->
                                <div class="col-lg-12">
                                    <canvas id="pegawai-hari-pie"></canvas>
                                </div>
                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-6">
                    <!-- /.panel -->
                    <div class="panel panel-default">
                        <div class="panel-heading row" style="margin-right: 0px; margin-left: 0px">
                            <div class="col-sm-12" style="padding-top: 8px"><i class="fa fa-bar-chart-o fa-fw"></i> Performance Karyawan per Bulan</div>
                            <div class="col-sm-12">
                                <div class="pull-right">
                                    <div class="input-group date">
                                        <input type="text" name="tanggal-chart-bulan" id="tanggal-chart-bulan" class="form-control datepickerbulan">
                                        <label class="input-group-addon" for="tanggal-chart-bulan">
                                            <span class="glyphicon glyphicon-th"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body" style="min-height: 10px;">
                            <div class="row">
                                <!-- /.col-lg-4 (nested) -->
                                <div class="col-lg-12">
                                    <canvas id="pegawai-month-donut"></canvas>
                                </div>
                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>

                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading row" style="margin-right: 0px; margin-left: 0px">
                            <div class="col-sm-9" style="padding-top: 8px"><i class="fa fa-bar-chart-o fa-fw"></i> Total Call Semua Karyawan per Bulan</div>
                            <div class="col-sm-3">
                                <div class="pull-right">
                                    <div class="input-group date">
                                        <input type="text" name="tanggal-chart-bulanan" id="tanggal-chart-bulanan" class="form-control datepickerbulan">
                                        <label class="input-group-addon" for="tanggal-chart-bulanan">
                                            <span class="glyphicon glyphicon-th"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body" style="min-height: 10px;">
                            <div class="row">
                                <!-- /.col-lg-4 (nested) -->
                                <div class="col-lg-12">
                                    <canvas id="pegawai-bulan-line"></canvas>
                                </div>
                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    

</body>

</html>
