<?php 
   class PositionModel extends CI_Model {
	
      function __construct() { 
         parent::__construct(); 
      }

      public function get_position(){
         $query = $this->db->get('position');
         return $query->result();
      }

      public function detail($id) { 
         $sql = 'select * from position where id = ?';
         $query = $this->db->query($sql, array($id));
         $result = $query->row(); 
         return $result;
      }

      public function add_jabatan($data){
         $this->db->insert('position', $data);
      }

      public function delete_jabatan($id){
         $this->db->where('id', $id);
         $this->db->delete('position'); 
      }  

      public function edit_jabatan($id, $data){
         $this->db->where('id', $id);
         $this->db->update('position', $data); 
      }
   
   } 