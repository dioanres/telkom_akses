<!DOCTYPE html>
<html lang="en">
<body>

    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Data Pegawai</h1>
                </div>
                <div>
                       
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           <button type="button" class="btn btn-primary" id="btn-tambah-pegawai" data-toggle="modal" data-target="#modal-pegawai">Tambah Data</button> 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped" id="tabel-pegawai">
                                <thead>
                                    <tr align="center">
                                        <th>No</th>
                                        <th>ID</th>
                                        <th>Nik</th>
                                        <th>Nama</th>
                                        <th>No. KTP</th>
                                        <th text-align="center">Foto</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                                       
                    <!-- /.panel -->
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    

</body>

</html>
