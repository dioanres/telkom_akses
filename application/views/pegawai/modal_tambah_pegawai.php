
<div class="modal fade" id="modal-pegawai" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
             <form method="post" id="form-pegawai"  enctype="multipart/form-data">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Form Tambah Data</h4>
            </div>
            <div class="modal-body">
               
                <div class="row">
                    <div class="form-group col-lg-12" hidden="hidden">
                        <div class="col-lg-4">
                            <label>Nama Pegawai </label>
                        </div>
                        <div class="col-lg-8">
                             <input class="form-control" id="id-pegawai" name="id_pegawai" hidden="hidden">       
                        </div>
                    </div>
                    <div class="form-group col-lg-12">
                        <div class="col-lg-4">
                            <label>Nama Pegawai <span class="text-danger">*</span></label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" id="nama-pegawai" name="nama_pegawai" placeholder="Nama Pegawai">        
                        </div>
                    </div>
                    <div class="form-group col-lg-12">
                        <div class="col-lg-4">
                            <label>NIK <span class="text-danger">*</span></label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" id="nik" name="nik" placeholder="NIK">        
                        </div>
                    </div>
                    <div class="form-group col-lg-12">
                        <div class="col-lg-4">
                            <label>No KTP <span class="text-danger">*</span></label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" id="no-ktp" name="no_ktp" placeholder="No KTP">        
                        </div>
                    </div>
                    <div class="form-group col-lg-12">
                        <div class="col-lg-4">
                            <label>Alamat</label>
                        </div>
                        <div class="col-lg-8">
                           <textarea class="form-control" id="alamat" name="alamat" rows="3"></textarea>        
                        </div>
                    </div>
                     
                    <div class="form-group col-lg-12">
                        <div class="col-lg-4">
                            <label>Tempat Lahir</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" id="tempat-lahir" name="tempat_lahir" placeholder="Tempat Lahir">
                        </div>
                    </div>

                     <div class="form-group col-lg-12">
                        <div class="col-lg-4">
                            <label>Tanggal Lahir</label>
                        </div>
                       <!-- <div class="input-group">
                            <input class="form-control date-picker" id="id-date-picker-1" type="text" data-date-format="dd-mm-yyyy" />
                            <span class="input-group-addon">
                                <i class="fa fa-calendar bigger-110"></i>
                            </span>
                        </div> -->
                       
                        <div class="col-lg-8">
                            <div class="input-group date col-lg-8" data-provide="datepicker">
                                <input type="text" class="form-control datepicker" name="tanggal_lahir" id="tanggal-lahir" data-date-format="dd/mm/yyyy">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>        
                        </div>
                    </div>
                    
                    <div class="form-group col-lg-12">
                        <div class="col-lg-4">
                            <label>No HP <span class="text-danger">*</span></label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" id="no-hp" name="no_hp" placeholder="No HP">        
                        </div>
                    </div>
                     <div class="form-group col-lg-12">
                        <div class="col-lg-4">
                            <label>Jenis Kelamin</label>
                        </div>
                        <div class="col-lg-8">
                           <div class="radio">
                                <label>
                                    <input type="radio" name="jenis_kelamin" id="optionsRadios1" value="L" checked class="jk">Laki-Laki
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="jenis_kelamin" id="optionsRadios2" value="P" class="jk">Perempuan
                                </label>
                            </div>      
                        </div>
                    </div>
                    <div class="form-group col-lg-12">
                        <div class="col-lg-4">
                            <label>Status Pegawai <span class="text-danger">*</span></label>
                        </div>
                        <div class="col-lg-8">
                             <select class="form-control" id="employee_status" name="employee_status">
                                <option value="">Pilih</option>
                                <option value="in" checked>Insource</option>
                                <option value="out">Outsource</option>
                            </select>        
                        </div>
                    </div>
                    <div class="form-group col-lg-12">
                        <div class="col-lg-4">
                            <label>Role <span class="text-danger">*</span></label>
                        </div>
                        <div class="col-lg-8">
                             <select class="form-control" id="role" name="role">
                                <option value="">Pilih</option>
                                <option value="2">User</option>
                                <option value="1">Admin</option>
                            </select>        
                        </div>
                    </div>
                    <div class="form-group col-lg-12" id="div-password" hidden="hidden">
                        <div class="col-lg-4">
                            <label>Password <span class="text-danger">*</span></label>
                        </div>
                        <div class="col-lg-8">
                            <input type="password" class="form-control" id=password name="password" placeholder="Password">        
                        </div>
                    </div>
                     <div class="form-group col-lg-12" id="div-password2" hidden="hidden">
                        <div class="col-lg-4">
                            <label>Konfirmasi Password <span class="text-danger">*</span></label>
                        </div>
                        <div class="col-lg-8">
                            <input type="password" class="form-control" id=password2 name="password2" placeholder="Konfirmasi Password">        
                        </div>
                    </div>
                    <div class="form-group col-lg-12">
                        <div class="col-lg-4">
                            <label>Foto</label>
                        </div>
                        <div class="col-lg-8">
                            <div class="col-sm-8">
                                <input type="file" id="foto" name="foto">
                                <input type="text" name="nama-foto" id="nama-foto" hidden="hidden">
                                <div id="preview1"></div>
                                <p class="text-info">file:JPG/JPEG/PNG Max:2MB</p>    
                            </div>
                                
                            <div class="col-sm-4">
                                <button type="button" class="btn btn-primary" id="upload-foto" name="upload-foto" >Upload</button>
                            </div>      
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="save-pegawai" name="save" >Save</button>
            </div>
            </form>
             <div id="err"></div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal