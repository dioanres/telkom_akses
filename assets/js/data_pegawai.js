

var last_path = window.location.href.split("/").pop();
console.log(base_url);
var $data='';
if (last_path==="view_pegawai_insource" || last_path==="view_pegawai_insource#") {
    $data = 'list_pegawai'
} else if (last_path==="view_pegawai_outsource"||last_path==="view_pegawai_outsource#") {
    $data = 'list_pegawai_outsource'
}
var tabel_pegawai = $('#tabel-pegawai').DataTable({
    "ajax":base_url+"pegawai/PegawaiController/"+$data,
    "dataSrc":"data",
    "info":false,
    "columns": [
        {"data":"nomor"},
        { "data": "id" },
        { "data": "employee_id" },
        { "data": "fullname" },
        { "data": "id_card_no" },
        { "data": "image", render: getImg }
        ],
    "columnDefs": [
        {
            "targets": [ 0 ],
            "data": 1
        },
        {
            "targets":[1],
            "visible":false
        },
        {
            "targets": [6],
            "data":null,
            "defaultContent": "<a class='green' href='#' ><i class='ace-icon fa fa-pencil bigger-130 col-sm-2 dtl-pegawai'></i></a><a class='red' href='#'><i class='ace-icon fa fa-trash-o bigger-130 col-sm-2 delete-pegawai'></i></a>"
        }
            
    ]
});


$('#role').change(function(){
    if ($('#role').val()==='1') {
        $('#div-password').show();
        $('#div-password2').show();
    } else {
         $('#div-password').hide();
         $('#div-password2').hide();
    }
});

$('#div-password2').on('Blur',function(){
    pass1 = $('#password').val();
    pass2 = $('password2').val();
    if (pass1 !== pass2) {
        console.log(pass1);
        console.log(pass2);
         toastr.warning('Password Yang di Masukkan Tidak Sesuai');
        //$(".error").html(").fadeIn();
    }
});

function getImg(data, type, full, meta) {
    if (data) {
        return '<img src="'+base_url+data+'" height="100px" width="100px" />';
    } else {
        return '';
    }
    
}
function no(data){
    console.log(data);
    $no = 0 ;
    $no = $no +1;
};

var arr_data_pegawai =[];
$('#tabel-pegawai tbody').on('click', '.dtl-pegawai, .delete-pegawai', function() {
        $(this).addClass('selected');
        if (typeof tabel_pegawai.row($(this).closest('tr')).data() !== 'undefined') {
            arr_data_pegawai = tabel_pegawai.row($(this).closest('tr')).data();
        } else if (typeof tabel_pegawai.row(this).data() !== 'undefined') {
            arr_data_pegawai = tabel_pegawai.row(this).data();
        } else {
            //arr_data_pegawai = tabel_pegawai.row($(this).parent().parent().parent().parent().parent().prev()).data();
            arr_data_pegawai = tabel_pegawai.columns(  ).row($(this).closest('tr')).data();
        }
        if ($(this).attr('class').includes('dtl-pegawai')) {
            console.log(this);
            
            $('#modal-pegawai').modal('show');

            //$('.modal').modal({backdrop: 'static', keyboard: false}) ;
            ajax_get_pegawai_by_id(arr_data_pegawai['id']);
        } else {
             swal({
              title: "Apakah Anda Yakin Ingin Menghapus Data "+arr_data_pegawai[3]+"?",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((confirm) => {
              if (confirm) {
                ajax_delete_pegawai(arr_data_pegawai['id']);
              }
            });
        }
});

$('#btn-tambah-pegawai').click(function(){
    $('#form-pegawai').find('input:text, input:password,input:file select, textarea,img,src').val('');
    $('#form-pegawai').find('input:radio, input:checkbox').prop('checked', false);
    $('#preview').remove();
});

$('#save-pegawai').click(function(){
	ajax_save_pegawai();
});

$('#btn-update-password').click(function(){
    ajax_update_password();
});

$('#upload-foto').click(function(){
    ajax_upload_foto();
});

function ajax_upload_foto(){
    $.ajax({
            url:  base_url + "pegawai/PegawaiController/upload_foto",
            type: "POST",
            data:  new FormData($('#form-pegawai')[0]),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function()
            {
                //$("#preview").fadeOut();
                $("#err").fadeOut();
            },
            success: function(data)
            {
                data = data.trim();
                console.log(data);
                if(data=='invalid')
                {
                    // invalid file format.
                    $("#err").html("Invalid File !").fadeIn();
                }
                else
                {
                    // view uploaded file.
                    console.log("<img src='"+base_url+data+"' width='100px' height='100px'/>");
                    
                    $("#preview1").html("<img src='"+base_url+data+"' width='100px' height='100px'/>");
                    console.log($('#preview').html());
                    $('#nama-foto').val(data);
                    
                     //$("#preview").html("<img src='"+base_url+response['image']+"' width='100px' height='100px'/>").fadeIn();
                    //$("#form")[0].reset();  
                }
            },
            error: function(e) 
            {
                $("#err").html(e).fadeIn();
            }           
       });
}
function ajax_save_pegawai() {
	$.ajax({
            method:"POST",
            url:base_url + "pegawai/PegawaiController/save_pegawai",
            data:$("#form-pegawai").serialize(),
            cache:false,
            success:function(response) {
                console.log($.trim(response));
               if ($.trim(response) == '1') {
                    $('#modal-pegawai').modal('hide');
                    //console.log(swal());
                    //alert('Berhasil');
                    swal("Berhasil Menyimpan Data").then((value) => {
                        location.reload();
                    });
                }else{
                    toastr.warning(response);
                }
            },
            error: function(response) {
                 console.log(response);
                //alert(response);
                toastr.warning(response);
            }
        });
}

function ajax_update_password() {
    console.log($("#form-update-password").serialize());
    $.ajax({
            method:"POST",
            url:base_url + "pegawai/PegawaiController/update_password",
            data:$("#form-update-password").serialize(),
            cache:false,
            success:function(response) {
                console.log($.trim(response));
               if ($.trim(response) == '1') {
                    $('#modal-pegawai').modal('hide');
                    //console.log(swal());
                    //alert('Berhasil');
                    swal("Berhasil Menyimpan Data").then((value) => {
                        location.reload();
                    });
                }else{
                    toastr.warning(response);
                }
            },
            error: function(response) {
                 console.log(response);
                //alert(response);
                toastr.warning(response);
            }
        });
}

function ajax_get_pegawai_by_id(id) {
    $.ajax({
            url: base_url + "pegawai/PegawaiController/get_pegawai_by_id",
            type: 'post',
            data: {
                id: id
                
            },
            cache: false,
            success: function(response, status, error, pesan) {
            console.log(response); 
                $('#id-pegawai').val(id);       
                $('#nama-pegawai').val(response['fullname']);
                $('#nik').val(response['employee_id']);
                $('#no-ktp').val(response['id_card_no']);
                $('#alamat').val(response['address']);
                $('#tempat-lahir').val(response['birth_place']);
                $('#tanggal-lahir').val(response['birth_date']);
                $('#no-hp').val(response['no_handphone']);
                $('#nama-foto').val(response['image']);
                $('.jk').val(response['jenis_kelamin']);
                $('#role').val(response['role_id']);
                $('#username').val(response['username']);
                $('#password').val(response['password']);
                $('#employee_status').val(response['employee_status']);
                $("#preview1").html("<img src='"+base_url+response['image']+"' width='100px' height='100px'/>").fadeIn();
            },
            error: function(response) {
                console.log(response);
                console.log(response['responseText']);
            }
        });
}

function ajax_delete_pegawai(id) {
    //tabel_guru.clear().draw()
    console.log(id);
    $.ajax({
        url: base_url+"pegawai/PegawaiController/delete_pegawai",
        dataType: 'json',
        type: 'post',
        data: { id: id },
        cache: false,
        async: false,
        success: function(response, status, error, pesan) {
            console.log(response);
            console.log(swal());
             swal("Data Berhasil Dihapus").then((value) => {
                location.reload();
            });
        },
        error: function(response) {
            console.log(response);
            alert(response);
            toastr.warning('Error', response['responseText']);

        }
    });
}