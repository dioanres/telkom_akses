<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class StrukturPegawaiController extends CI_Controller {

	function __construct(){
		parent::__construct();
        $this->load->model('StrukturPegawaiModel');
        $this->load->model('PegawaiModel');
        $this->load->model('PositionModel');
        $this->load->library('form_validation');
	}

	public function index()
	{
		$data['employee'] = $this->PegawaiModel->get_pegawai();
		$data['position'] = $this->PositionModel->get_position();
		$this->load->view('header');
		$this->load->view('pegawai/struktur_pegawai.php', $data);
		$this->load->view('footer');
	}

	public function get_struktur(){
		$data = $this->StrukturPegawaiModel->get_struktur();
		foreach ($data as $key => $value) {
			$value->Image = '../../'.$value->Image;
		}
		$this->output->set_output(json_encode($data));
	}

	public function add_struktur(){
		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_rules('employee', 'Nama Pegawai', 'required');
		$this->form_validation->set_rules('position', 'Jabatan', 'required');
		if ($this->form_validation->run() != FALSE){
            $data = array(
				'employee_id' => $this->input->post('employee'),
				'position_id' => $this->input->post('position'),
				'parent_id' => $this->input->post('parent')
			);

			$this->StrukturPegawaiModel->add_struktur($data);
			$this->output->set_output('berhasil');
        }else{
        	$this->output->set_output(validation_errors());
        }
        
	}

	public function edit_struktur(){
		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_rules('employee', 'Nama Pegawai', 'required');
		$this->form_validation->set_rules('position', 'Jabatan', 'required');
		if ($this->form_validation->run() != FALSE){
            $data = array(
				'employee_id' => $this->input->post('employee'),
				'position_id' => $this->input->post('position')
			);
			$this->StrukturPegawaiModel->edit_struktur($this->input->post('id'), $data);
			$this->output->set_output('berhasil');
        }else{
        	$this->output->set_output(validation_errors());
        }
        
	}

	public function delete_struktur(){
		$id = $this->input->post('id');
		$this->StrukturPegawaiModel->delete_struktur($id);
		$this->output->set_output('berhasil');
	}

	public function detail(){
		$id = $this->input->post('id');
		$data = $this->StrukturPegawaiModel->detail($id);
		$this->output->set_output(json_encode($data));
	}

	function _notMatch($value1, $value2){
	   if($value1 == $this->input->post($value2)){
	       $this->form_validation->set_message('_notMatch', 'Pegawai dan Atasan harus berbeda');
	       return false;
	   }
	   return true;
	}
	
}