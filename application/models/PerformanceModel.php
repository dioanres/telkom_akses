<?php 
   class PerformanceModel extends CI_Model {
      function __construct() { 
         parent::__construct(); 
      } 
   
      public function get_performance() { 
         $sql = 'SELECT ep.id, ep.no_sc, ep.no_telpon_pelanggan, ep.nama_pelanggan, e.fullname, ep.employee_id
                  FROM employee_performance ep
                  LEFT JOIN employee e on e.id = ep.employee_id';
         $query = $this->db->query($sql);
         $result = $query->result(); 
         return $result;
      }

      public function performance_day($date = null, $date_to = null){
         if($date_to == null){
            $sql = 'SELECT COUNT(*) "value", e.fullname "label" from employee_performance ep LEFT join employee e on e.id = ep.employee_id where e.is_active = 1 and cast(ep.created_date as date) = ? GROUP BY 2';
         }else{
            $sql = 'SELECT COUNT(*) "value", e.fullname "label" from employee_performance ep LEFT join employee e on e.id = ep.employee_id where e.is_active = 1 and cast(ep.created_date as date) BETWEEN ? and ? GROUP BY 2';
         }
         $query = $this->db->query($sql, array($date, $date_to));
         $result = $query->result(); 
         return $result;  
      }

      public function performance_daily($date = null){
         $sql = "SELECT cast(DATE_FORMAT(a.Date, '%d')as int) label, IFNULL(perform.jumlah, 0) value
               from (
                   select last_day(DATE_ADD(NOW(), INTERVAL 1 YEAR)) - INTERVAL (a.a + (10 * b.a) + (100 * c.a)) DAY as Date
                   from (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as a
                   cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as b
                   cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as c
               ) a
               LEFT JOIN (SELECT COUNT(*) jumlah, DATE_FORMAT(ep.created_date, '%Y-%m-%d') bulan FROM employee_performance ep WHERE DATE_FORMAT(ep.created_date, '%Y-%m') = ?) perform on perform.bulan = a.Date
               WHERE DATE_FORMAT(a.Date, '%Y-%m') = ?
               ORDER BY 1";
         $query = $this->db->query($sql, array($date, $date));
         $result = $query->result(); 
         return $result;  
      }

      public function performance_month($date = null){
         $sql = 'SELECT COUNT(*) "value", e.fullname "label" from employee_performance ep LEFT join employee e on e.id = ep.employee_id where DATE_FORMAT(ep.created_date, \'%Y-%m\') = ? GROUP BY 2';
         $query = $this->db->query($sql, array($date));
         $result = $query->result(); 
         return $result;  
      }

      public function performance_year($date = null){
         $sql = "SELECT IFNULL(jumlah, 0) value, months.bulan label FROM (SELECT 1 m, 'Januari' bulan UNION ALL SELECT 2, 'Februari' UNION ALL SELECT 3, 'Maret' UNION ALL SELECT 4, 'April' UNION ALL SELECT 5, 'Mei' UNION ALL SELECT 6, 'Juni' UNION ALL SELECT 7, 'Juli' UNION ALL SELECT 8, 'Agustus' UNION ALL SELECT 9, 'September' UNION ALL SELECT 10, 'Oktober' UNION ALL SELECT 11, 'November' UNION ALL SELECT 12, 'Desember') months LEFT JOIN (SELECT COUNT(*) jumlah, cast(DATE_FORMAT(ep.created_date, '%m') as int) bulan FROM employee_performance ep WHERE DATE_FORMAT(ep.created_date, '%Y') = ?) perform on perform.bulan = months.m";
         $query = $this->db->query($sql, array($date));
         $result = $query->result(); 
         return $result;  
      }

      public function detail($id = null) { 
         $sql = 'SELECT eo.*, UPPER(e.fullname) "Name", UPPER(p.name) "Title", e.image "Image",  "Phone"
                  FROM employee_performance eo
                  LEFT JOIN employee e on e.id = eo.employee_id
                  LEFT JOIN position p on p.id = eo.position_id
                  WHERE eo.id = ?';
         $query = $this->db->query($sql, array($id));
         $result = $query->row(); 
         return $result;
      }

      public function add_performance($data){
         $this->db->insert('employee_performance', $data);
      }

      public function delete_performance($id){
         $this->db->where('id', $id);
         $this->db->delete('employee_performance'); 
      }  

      public function edit_performance($id, $data){
         $this->db->where('id', $id);
         $this->db->update('employee_performance', $data); 
      }  
   }