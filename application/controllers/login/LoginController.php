<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start();
class LoginController extends CI_Controller {

	public function __construct()
	{
		parent:: __construct();

		$this->load->library('session');
		$this->load->helper(array('form','url'));
		$this->load->library('form_validation');
		$this->load->model('LoginModel');
	}

	public function index()
	{
		$this->load->view('v_login');
	}

	public function proses_login()
	{
		$this->form_validation->set_rules('nik', 'Nik', 'trim|xss_clean|callback_username_check');
		$this->form_validation->set_rules('password', 'Password');
		if($this->form_validation->run() == FALSE)
		{
			$this->load->view('v_login');
		} else
			if($this->session->userdata('akses'))
			{
				redirect(site_url('home/HomeController'), 'refresh');
			}
			else
			{
				redirect(site_url('home/HomeController'), 'refresh');
			}
		}

	public function username_check($user)
	{
		$pass = $this->input->post('password');
		if($user == '' && $pass == '')
		{
			$this->form_validation->set_message('username_check', '<div class="alert alert-danger text-center">Username dan Password Harus Diisi</div>');
			return FALSE;
		}
		elseif($user == '' || $user == null)
		{
			$this->form_validation->set_message('username_check', '<div class="alert alert-danger text-center">Nik harus diisi</div>');
			return FALSE;
		}
		elseif ($pass == '' || $pass == null) {
			$this->form_validation->set_message('username_check', '<div class="alert alert-danger text-center">Password harus diisi</div>');
			return FALSE;
		}
		else
		{		
			$data = $this->LoginModel->proses_login($user);
			if (empty($data)) {
				$this->form_validation->set_message('username_check', '<div class="alert alert-danger text-center">Anda Belum Terdaftar Sebagai Karyawan</div>');
				return FALSE;
			} else {
				if (empty($data)) {
					$this->form_validation->set_message('username_check', '<div class="alert alert-danger text-center">Anda Tidak Memiliki Akses Untuk Login</div>');
					return FALSE;
				} else {
					if ($data->password==md5($pass)) {
					$this->session->set_userdata('session_pegawai', $data);
					} else {
						$this->form_validation->set_message('username_check', '<div class="alert alert-danger text-center">Password Tidak Sesuai, Mohon Coba Kembali</div>');
						return FALSE;
					}
				}
				
			}
		}
	}

	public function logout() {
		$this->session->unset_userdata('session_pegawai');
		$this->session->sess_destroy();
		redirect(site_url('login/LoginController'), 'refresh');
	}

	public function akses() {
		$data = $this->session->userdata('akses_login');
		$this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($data));
	}
	public function enk(){
		$a = 'admin';
		$b = base64_encode($a);
		$c = base64_decode($b);
		echo 'enkripdi'.$b;
		echo '<br/>dekripsi'.$c;
	}
}
