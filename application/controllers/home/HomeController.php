<?php 
session_start();

class HomeController extends CI_Controller {

	public function __construct()
	{
		parent:: __construct();

		$this->load->library('session');
		$this->load->helper(array('form','url'));
		$this->load->library('form_validation');
		
	}
	public function index()
	{	
		if ($this->session->userdata('session_pegawai')) {
			$this->load->view('header');
			$this->load->view('home');
			$this->load->view('footer');
		} else {
			redirect(site_url('login/LoginController'), 'refresh');
		}
	}

	public function homepage()
	{
		$this->load->view('header');
		$this->load->view('home');
		$this->load->view('footer');
	}
}
