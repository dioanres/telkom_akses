getOrgChart.themes.CustomTheme =
{
    size: [330, 260],
    toolbarHeight: 46,
    textPoints: [
        { x: 20, y: 45, width: 300 },
        { x: 120, y: 100, width: 200 },
        { x: 120, y: 125, width: 200 }
    ],
    textPointsNoImage: [
        { x: 20, y: 45, width: 300 },
        { x: 120, y: 100, width: 200 },
        { x: 120, y: 125, width: 200 }
    ],
    box: '<rect x="0" y="0" height="260" width="330" rx="10" ry="10" class="get-box"></rect>'
        + '<g transform="matrix(0.25,0,0,0.25,123,142)"><path d="M48.014,42.889l-9.553-4.776C37.56,37.662,37,36.756,37,35.748v-3.381c0.229-0.28,0.47-0.599,0.719-0.951  c1.239-1.75,2.232-3.698,2.954-5.799C42.084,24.97,43,23.575,43,22v-4c0-0.963-0.36-1.896-1-2.625v-5.319  c0.056-0.55,0.276-3.824-2.092-6.525C37.854,1.188,34.521,0,30,0s-7.854,1.188-9.908,3.53C17.724,6.231,17.944,9.506,18,10.056  v5.319c-0.64,0.729-1,1.662-1,2.625v4c0,1.217,0.553,2.352,1.497,3.109c0.916,3.627,2.833,6.36,3.503,7.237v3.309  c0,0.968-0.528,1.856-1.377,2.32l-8.921,4.866C8.801,44.424,7,47.458,7,50.762V54c0,4.746,15.045,6,23,6s23-1.254,23-6v-3.043  C53,47.519,51.089,44.427,48.014,42.889z M51,54c0,1.357-7.412,4-21,4S9,55.357,9,54v-3.238c0-2.571,1.402-4.934,3.659-6.164  l8.921-4.866C23.073,38.917,24,37.354,24,35.655v-4.019l-0.233-0.278c-0.024-0.029-2.475-2.994-3.41-7.065l-0.091-0.396l-0.341-0.22  C19.346,23.303,19,22.676,19,22v-4c0-0.561,0.238-1.084,0.67-1.475L20,16.228V10l-0.009-0.131c-0.003-0.027-0.343-2.799,1.605-5.021  C23.253,2.958,26.081,2,30,2c3.905,0,6.727,0.951,8.386,2.828c1.947,2.201,1.625,5.017,1.623,5.041L40,16.228l0.33,0.298  C40.762,16.916,41,17.439,41,18v4c0,0.873-0.572,1.637-1.422,1.899l-0.498,0.153l-0.16,0.495c-0.669,2.081-1.622,4.003-2.834,5.713  c-0.297,0.421-0.586,0.794-0.837,1.079L35,31.623v4.125c0,1.77,0.983,3.361,2.566,4.153l9.553,4.776  C49.513,45.874,51,48.28,51,50.957V54z" fill="#FFFFFF"/></g>'
        + '<g transform="matrix(1,0,0,1,20,190)" class="btns" data-action="edit"><path d="M5 0 L97 0 Q97 0 97 0 L97 45 Q97 45 97 45 L5 45 Q0 45 0 40 L0 5 Q0 0 5 0 Z"></path><text x="35" y="30" font-size="20px" width="60">Edit</text></g>'
        + '<g transform="matrix(1,0,0,1,117,190)" class="btns" data-action="add"><path d="M0 0 L97 0 Q97 0 97 0 L97 45 Q97 45 97 45 L0 45 Q0 45 0 45 L0 0 Q0 0 0 0 Z"></path><text x="35" y="30" font-size="20px" width="60">Add</text></g>'
        + '<g transform="matrix(1,0,0,1,214,190)" class="btns" data-action="delete"><path d="M0 0 L92 0 Q97 0 97 5 L97 40 Q97 45 92 45 L0 45 Q0 45 0 45 L0 0 Q0 0 0 0 Z"></path><text font-size="20px" x="20" y="30" width="60">Delete</text></g>',
    text: '<text width="[width]" class="get-text get-text-[index]" x="[x]" y="[y]">[text]</text>',
    image: '<clipPath id="clip"><circle cx="60" cy="120" r="40" /></clipPath><image class="imgs" preserveAspectRatio="xMidYMid slice" clip-path="url(#clip)" xlink:href="[href]" x="20" y="80" height="80" width="80"/>',
    expandCollapseBtnRadius: 20
};

var detail_struktur = '';

function get_detail(id){
	$.ajax({
	  method: "POST",
	  url: base_url + "pegawai/StrukturPegawaiController/detail",
	  async: false,
	  data: { 
		id: id
	  },
	  success: function(response){
	  	detail_struktur = $.parseJSON(response);
	  }
	});
}

function new_struktur(){
	$.ajax({
	  method: "POST",
	  url: base_url + "pegawai/StrukturPegawaiController/add_struktur",
	  data: { 
		employee: $('#nemployee').val(),
		position: $('#nposition').val(),
		parent: 0
	  },
	  success: function(response){
	  	if (response == 'berhasil') {
	  		$('#modalAdd').modal('hide');
	  		swal("Berhasil menambah Top Level").then((value) => {
			 	location.reload();
			});
	  	}else{
	  		toastr.warning(response);
	  	}
	  }
	});
}

function add_struktur(){
	$.ajax({
	  method: "POST",
	  url: base_url + "pegawai/StrukturPegawaiController/add_struktur",
	  data: { 
		employee: $('#employee').val(),
		position: $('#position').val(),
		parent: $('#parent').val() 
	  },
	  success: function(response){
	  	if (response == 'berhasil') {
	  		$('#modalAdd').modal('hide');
	  		swal("Berhasil menambah karyawan").then((value) => {
			 	location.reload();
			});
	  	}else{
	  		toastr.warning(response);
	  	}
	  }
	});
}

function edit_struktur(){
	$.ajax({
	  method: "POST",
	  url: base_url + "pegawai/StrukturPegawaiController/edit_struktur",
	  data: { 
		employee: $('#eemployee').val(),
		position: $('#eposition').val(),
		id: $('#eid').val()
	  },
	  success: function(response){
	  	if (response == 'berhasil') {
	  		$('#modalEdit').modal('hide');
	  		swal("Berhasil mengubah karyawan").then((value) => {
			 	location.reload();
			});
	  	}else{
	  		toastr.warning(response);
	  	}
	  }
	});
}

function delete_struktur(id){
	$.ajax({
	  method: "POST",
	  url: base_url + "pegawai/StrukturPegawaiController/delete_struktur",
	  data: { 
		id: id
	  },
	  success: function(response){
	  	if (response == 'berhasil') {
	  		swal("Berhasil Menghapus Karyawan pada Struktur Organisasi").then((value) => {
			 	location.reload();
			});
	  	}else{
	  		toastr.warning(response);
	  	}
	  }
	});
}

$.getJSON(base_url+"pegawai/StrukturPegawaiController/get_struktur", function (source) {
	try{
		var peopleElement = document.getElementById("people");
		var orgChart = new getOrgChart(peopleElement, {
		    theme: "CustomTheme",                
		    primaryFields: ["Name", "Title"],
	        photoFields: ["Image"],
		    enableGridView: false,
		    enableZoom: false,
		    enableExportToImage: false,
		    enableEdit: false,
		    enableDetailsView: false,
		    scale: 0.5,
		    renderNodeEvent: renderNodeEvent,
		    dataSource: source
		});
	}catch(e){
		$('#modalNew').modal('show');
	}
	

	function renderNodeEvent(sender, args) {
		console.log(sender,args);
		args.content[2] = args.content[2].replace("Amber McKenzie", "The name has been modified");
    }

	function getNodeByClickedBtn(el) {
	    while (el.parentNode) {
	        el = el.parentNode;
	        if (el.getAttribute("data-node-id"))
	            return el;
	    }
	    return null;
	}

	var init = function () {
	    var btns = document.getElementsByClassName("btns");
	    for (var i = 0; i < btns.length; i++) {

	        btns[i].addEventListener("click", function () {
	            var nodeElement = getNodeByClickedBtn(this);
	            var action = this.getAttribute("data-action");
	            var id = nodeElement.getAttribute("data-node-id");
	            var node = orgChart.nodes[id];

	            // crud button
	            switch (action) {
	                case "add":
	                	$('#modalAdd').modal('show');
	                	$('#parent').val(id);
	                    break;
	                case "edit":
	                    $('#modalEdit').modal('show');
	                    get_detail(id);
	                    $('#eemployee').val(detail_struktur['employee_id']);
	                    $('#eposition').val(detail_struktur['position_id']);
	                    $('#eid').val(id);
	                    break;
	                case "delete":
	                	get_detail(id);
		                swal({
						  title: "Apakah Anda Yakin Ingin Menghapus "+detail_struktur['Name']+" sebagai "+detail_struktur['Title']+"?",
						  icon: "warning",
						  buttons: true,
						  dangerMode: true,
						})
						.then((confirm) => {
						  if (confirm) {
						    delete_struktur(id);
						  }
						});
	                    break;
	            }
	        });
	    }
	}

	init();
});

$(document).ready(function(){
	$('#save').click(function(){
		swal({
		  title: "Apakah Anda Yakin Ingin Menambahkan "+$('#employee :selected').html()+" sebagai "+$('#position :selected').html()+"?",
		  icon: "warning",
		  buttons: true,
		  dangerMode: true,
		})
		.then((confirm) => {
		  if (confirm) {
		    add_struktur();
		  }
		});
	});

	$('#nsave').click(function(){
		swal({
		  title: "Apakah Anda Yakin Ingin Menambahkan "+$('#nemployee :selected').html()+" sebagai "+$('#nposition :selected').html()+"?",
		  icon: "warning",
		  buttons: true,
		  dangerMode: true,
		})
		.then((confirm) => {
		  if (confirm) {
		    new_struktur();
		  }
		});
	});

	$('#esave').click(function(){
		swal({
		  title: "Apakah Anda Yakin Ingin Mengubah menjadi "+$('#eemployee :selected').html()+" sebagai "+$('#eposition :selected').html()+"?",
		  icon: "warning",
		  buttons: true,
		  dangerMode: true,
		})
		.then((confirm) => {
		  if (confirm) {
		    edit_struktur();
		  }
		});
	});
});