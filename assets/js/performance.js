function new_performance(){
	$.ajax({
	  method: "POST",
	  url: base_url + "pegawai/PerformanceController/add_performance",
	  data: new FormData($('#form-performance')[0]),
	  processData: false,
        contentType: false,
	  success: function(response){
	  	if (response == 'berhasil') {
	  		$('#modal-performance').modal('hide');
	  		$("#form-performance")[0].reset();
	  		swal("Berhasil menambah data").then((value) => {
			 	// location.reload();
			 	list_performance();
			});
	  	}else{
	  		toastr.warning(response);
	  	}
	  }
	});
}

function edit_performance(){
	$.ajax({
	  method: "POST",
	  url: base_url + "pegawai/PerformanceController/edit_performance",
	  data: new FormData($('#form-edit-performance')[0]),
	  processData: false,
        contentType: false,
	  success: function(response){
	  	if (response == 'berhasil') {
	  		$('#modal-edit-performance').modal('hide');
	  		$("#form-edit-performance")[0].reset();
	  		swal("Berhasil mengubah data").then((value) => {
			 	// location.reload();
			 	list_performance();
			});
	  	}else{
	  		toastr.warning(response);
	  	}
	  }
	});
}

function list_performance() {
    tabel_performance.clear().draw()
    $.ajax({
        url: base_url + "pegawai/PerformanceController/get_performance",
        type: 'get',
        cache: false,
        //async: false,
        success: function(response, status, error, pesan) {
            response = $.parseJSON(response);
            var i = 1;
            var aksi = "<a class='green' href='#' ><i class='ace-icon fa fa-pencil bigger-130 col-sm-2 dtl-performance'></i></a><a class='red' href='#'><i class='ace-icon fa fa-trash-o bigger-130 col-sm-2 delete-performance'></i></a>";
            $.each(response, function() {
                //console.log("<img src='"+base_url+this['image']+"'");
                tabel_performance.row.add([
                    i,
                    this['fullname'],
                    this['no_sc'],
                    this['nama_pelanggan'],
                    this['no_telpon_pelanggan'],
                    aksi,
                    this['employee_id'],
                    this['id']
                ]).draw(false);
                i++;
            });

        },
        error: function(response) {
            console.log(response['responseText']);
        }
    });
}

function delete_performance(id){
	$.ajax({
	  method: "POST",
	  url: base_url + "pegawai/PerformanceController/delete_performance",
	  data: { 
		id: id
	  },
	  success: function(response){
	  	if (response == 'berhasil') {
	  		swal("Berhasil Menghapus Data").then((value) => {
			 	list_performance();
			});
	  	}else{
	  		toastr.warning(response);
	  	}
	  }
	});
}

var tabel_performance = $('#tabel-performance').DataTable({});
var pegawai_hari_bar = new Chart(document.getElementById('pegawai-hari-bar').getContext('2d'), {
        type: 'line',
        options: {
          responsive: true,
          maintainAspectRatio: false
        },
        data: {
            labels: [],
            datasets: [{
                data: [],
                label: 'Jumlah Call',
                borderWidth: 1
            }]
        }
    });

var pegawai_month_line = new Chart(document.getElementById('pegawai-bulan-line').getContext('2d'), {
        type: 'line',
        options: {
          responsive: true,
          maintainAspectRatio: false
        },
        data: {
            labels: [],
            datasets: [{
                data: [],
                label: 'Jumlah Call',
                borderWidth: 1
            }]
        }
    });

var pegawai_month_pie = new Chart(document.getElementById('pegawai-month-donut').getContext('2d'), {
        type: 'line',
        options: {
          responsive: true,
          maintainAspectRatio: false
        },
        data: {
            labels: [],
            datasets: [{
                data: [],
                label: 'Jumlah Call',
                borderWidth: 1
            }]
        }
    });

var pegawai_hari_pie = new Chart(document.getElementById('pegawai-hari-pie').getContext('2d'), {
        type: 'line',
        options: {
          responsive: true,
          maintainAspectRatio: false
        },
        data: {
            labels: [],
            datasets: [{
                data: [],
                label: 'Jumlah Call',
                borderWidth: 1
            }]
        }
    });

var pegawai_tahun_area = new Chart(document.getElementById('pegawai-tahun-area').getContext('2d'), {
        type: 'line',
        options: {
          responsive: true,
          maintainAspectRatio: false
        },
        data: {
            labels: [],
            datasets: [{
                data: [],
                label: 'Jumlah Call',
                borderWidth: 1
            }]
        }
    });

$('#tanggal-chart-harian-sampai').datepicker({
    autoclose: true,
    todayHighlight: true,
    format: 'yyyy-mm-dd'
});

function chart_harian(){
    $.ajax({
        url: base_url + "pegawai/PerformanceController/performance_day2",
        type: 'POST',
        cache: false,
        data: {
            date: $('#tanggal-chart-harian').val(),
            to: $('#tanggal-chart-harian-sampai').val()
        },
        //async: false,
        success: function(response, status, error, pesan) {
            $('#pegawai-hari-bar').empty();
            var ctx = document.getElementById('pegawai-hari-bar').getContext('2d');
            var ctx_pie = document.getElementById('pegawai-hari-pie').getContext('2d');
            var label = response['label'];
            var data = response['value'];
            var backgroundColor = [];
            // $('#pegawai-hari-bar').attr('height', (data.length * 20) + 'px');
            pegawai_hari_bar.destroy();
            pegawai_hari_pie.destroy();
            if(data.length > 0){
                $.each(data, function(index){
                    backgroundColor.push(CSS_COLOR_NAMES[index]);
                });
                pegawai_hari_pie = new Chart(ctx_pie,{
                    type: 'pie',
                    data: {
                        labels: label,
                        datasets: [{
                            data: data,
                            backgroundColor: 'rgba(54, 162, 235, 0.5)',
                            borderColor: 'white',
                            label: 'Jumlah Call',
                            borderWidth: 2,
                        }]
                    },
                    options: {
                        legend: {
                            display: false
                         },
                         tooltips: {
                            enabled: false
                         },
                        plugins: {
                          datalabels: {
                             display: true,
                             align: 'center',
                             anchor: 'center',
                             color: '#FFF',
                             formatter: function(value, context) {
                                var nama = label[context.dataIndex]+' ';
                                return nama.substr(0, nama.indexOf(" ")) + ': ' + value;
                            }
                          }
                       }
                    }
                });
                pegawai_hari_bar = new Chart(ctx, {
                    type: 'bar',
                    options: {
                        plugins: {
                          datalabels: {
                             display: true,
                             align: 'end',
                             anchor: 'end',
                             color: '#000000'
                          }
                       },
                      responsive: true,
                      maintainAspectRatio: false,
                      scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    },
                    data: {
                        labels: label,
                        datasets: [{
                            data: data,
                            backgroundColor: 'rgba(54, 162, 235, 0.5)',
                            borderColor: 'rgba(54, 162, 235, 1)',
                            borderWidth: 1,
                            label: 'Jumlah Call',
                            borderWidth: 1
                        }]
                    }
                });
            }
            // $('#pegawai-hari-bar').attr('height', (data.length * 20) + 'px');
        },
        error: function(response) {
            console.log(response['responseText']);
        }
    });
}

function chart_tahunan(){
    $.ajax({
        url: base_url + "pegawai/PerformanceController/performance_year",
        type: 'POST',
        cache: false,
        data: {
            date: $('#tanggal-chart-tahun').val()
        },
        //async: false,
        success: function(response, status, error, pesan) {
            $('#pegawai-tahun-area').empty();
            var ctx = document.getElementById('pegawai-tahun-area').getContext('2d');
            var label = response['label'];
            var data = response['value'];
            // $('#pegawai-hari-bar').attr('height', (data.length * 20) + 'px');
            pegawai_tahun_area.destroy();
            if(data.length > 0){
                pegawai_tahun_area = new Chart(ctx, {
                    type: 'line',
                    options: {
                        plugins: {
                          datalabels: {
                             display: true,
                             align: 'end',
                             anchor: 'end',
                             color: '#000000'
                          }
                       },
                      responsive: true,
                      maintainAspectRatio: false,
                      scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    },
                    data: {
                        labels: label,
                        datasets: [{
                            data: data,
                            backgroundColor: 'rgba(54, 162, 235, 0.2)',
                            borderColor: 'rgba(54, 162, 235, 1)',
                            borderWidth: 1,
                            label: 'Jumlah Call',
                            borderWidth: 1
                        }]
                    }
                });
            }
            // $('#pegawai-hari-bar').attr('height', (data.length * 20) + 'px');
        },
        error: function(response) {
            console.log(response['responseText']);
        }
    });
}

function chart_bulanan(){
    $.ajax({
        url: base_url + "pegawai/PerformanceController/performance_month",
        type: 'POST',
        cache: false,
        data: {
            date: $('#tanggal-chart-bulan').val()
        },
        //async: false,
        success: function(response, status, error, pesan) {
            $('#pegawai-month-donut').empty();
            var ctx = document.getElementById('pegawai-month-donut').getContext('2d');
            var label = response['label'];
            var data = response['value'];
            // $('#pegawai-hari-bar').attr('height', (data.length * 20) + 'px');
            pegawai_month_pie.destroy();
            if(data.length > 0){
                pegawai_month_pie = new Chart(ctx,{
                    type: 'pie',
                    data: {
                        labels: label,
                        datasets: [{
                            data: data,
                            backgroundColor: 'rgba(54, 162, 235, 0.5)',
                            borderColor: 'white',
                            label: 'Jumlah Call',
                            borderWidth: 2,
                        }]
                    },
                    options: {
                        legend: {
                            display: false
                         },
                         tooltips: {
                            enabled: false
                         },
                        plugins: {
                          datalabels: {
                             display: true,
                             align: 'center',
                             anchor: 'center',
                             color: '#FFF',
                             formatter: function(value, context) {
                                var nama = label[context.dataIndex]+' ';
                                return nama.substr(0, nama.indexOf(" ")) + ': ' + value;
                            }
                          }
                       }
                    }
                });
            }
            // $('#pegawai-hari-bar').attr('height', (data.length * 20) + 'px');
        },
        error: function(response) {
            console.log(response['responseText']);
        }
    });
}

function chart_daily_bulanan(){
    $.ajax({
        url: base_url + "pegawai/PerformanceController/performance_daily",
        type: 'POST',
        cache: false,
        data: {
            date: $('#tanggal-chart-bulanan').val()
        },
        //async: false,
        success: function(response, status, error, pesan) {
            $('#pegawai-bulan-line').empty();
            var ctx = document.getElementById('pegawai-bulan-line').getContext('2d');
            var label = response['label'];
            var data = response['value'];
            // $('#pegawai-hari-bar').attr('height', (data.length * 20) + 'px');
            pegawai_month_line.destroy();
            if(data.length > 0){
                pegawai_month_line = new Chart(ctx, {
                    type: 'line',
                    options: {
                        plugins: {
                          datalabels: {
                             display: true,
                             align: 'end',
                             anchor: 'end',
                             color: '#000000'
                          }
                       },
                      responsive: true,
                      maintainAspectRatio: false,
                      scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    },
                    data: {
                        labels: label,
                        datasets: [{
                            data: data,
                            backgroundColor: 'rgba(54, 162, 235, 0.2)',
                            borderColor: 'rgba(54, 162, 235, 1)',
                            borderWidth: 1,
                            label: 'Jumlah Call',
                            borderWidth: 1
                        }]
                    }
                });
            }
            // $('#pegawai-hari-bar').attr('height', (data.length * 20) + 'px');
        },
        error: function(response) {
            console.log(response['responseText']);
        }
    });
}

if(controller == 'PerformanceController'){
	list_performance();
}

$(document).ready(function(){
    if(controller == 'HomeController'){
        chart_harian();
        chart_bulanan();
        chart_tahunan();
        chart_daily_bulanan();
    }
    $('#tanggal-chart-harian, #tanggal-chart-harian-sampai').on('changeDate', function (ev) {
        chart_harian();
    });
    $('#tanggal-chart-tahun').on('changeDate', function (ev) {
        chart_tahunan();
    });
    $('#tanggal-chart-bulan').on('changeDate', function (ev) {
        chart_bulanan();
    });
    $('#tanggal-chart-bulanan').on('changeDate', function (ev) {
        chart_daily_bulanan();
    });
	$('#save-performance').click(function(e){
		new_performance();
		e.preventDefault();
	});

	$('#edit-performance').click(function(e){
		edit_performance();
		e.preventDefault();
	});

	$('#tabel-performance').on('click', '.delete-performance, .dtl-performance', function(){
		if (typeof tabel_performance.row($(this).closest('tr')).data() !== 'undefined') {
            arr_data_performance = tabel_performance.row($(this).closest('tr')).data();
        } else if (typeof tabel_performance.row(this).data() !== 'undefined') {
            arr_data_performance = tabel_performance.row(this).data();
        } else {
            arr_data_performance = tabel_performance.row($(this).parent().parent().parent().parent().parent().prev()).data();
        }
        if ($(this).attr('class').includes('dtl-performance')) {
            $('#modal-edit-performance').modal('show');
            $('#enama').val(arr_data_performance[3]);
            $('#etlp').val(arr_data_performance[4]);
            $('#eemployee').val(arr_data_performance[6]);
            $('#esc').val(arr_data_performance[2]);
            $('#id-performance').val(arr_data_performance[7])
        } else {
            swal({
              title: "Apakah Anda Yakin Ingin Menghapus Data "+arr_data_performance[3]+"?",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((confirm) => {
              if (confirm) {
                delete_performance(arr_data_performance[7]);
              }
            });
        }
	});
})