<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="<?php echo base_url(); ?>assets/vendor/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/vendor/datatables-plugins/buttons.dataTables.min.css" rel="stylesheet">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url(); ?>assets/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>assets/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<?php echo base_url(); ?>assets/vendor/morrisjs/morris.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_url(); ?>assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <script src="<?php echo base_url(); ?>assets/vendor/getorgchart/getorgchart.js"></script>
    <link href="<?php echo base_url(); ?>assets/vendor/getorgchart/getorgchart.css" rel="stylesheet" />

    <link href="<?php echo base_url(); ?>assets/vendor/toastr/toastr.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/bootstrap/css/bootstrap-datepicker3.min.css">
</head>
<body>
	<div class="row">
		<div class="col-lg-12">
			<center><h3>Laporan Performance Pegawai</h3></center>
                 <!-- <div class="panel panel-default">
                    <div class="panel-body"> -->
                        <div class="table-responsive">
                            <table class="" border="1">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>NIK</th>
                                        <th>Nama</th>
                                        <th>No. HP</th>
                                        <th>No. SC</th>
                                        <th>Nama Pelanggan</th>
                                        <th>No. Telpon Pelanggan</th>
                                        <th>Tanggal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php $no = 1 ;
                                	foreach ($list as $key => $value) {
                                	 ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $value->employee_id; ?></td>
                                        <td><?php echo $value->fullname; ?></td>
                                        <td><?php echo $value->no_handphone; ?></td>
                                        <td><?php echo $value->no_sc; ?></td>
                                        <td><?php echo $value->nama_pelanggan; ?></td>
                                        <td><?php echo $value->no_telpon_pelanggan; ?></td>
                                        <td><?php echo $value->tanggal; ?></td>
                                    </tr>
                                    <?php $no++; } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    
		</div>
	</div>
</body>
</html>