<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Aplikasi PAB</title>
    <link href="<?php echo base_url(); ?>assets/dist/css/style-login.css" rel="stylesheet" />
    <style type="text/css">
        
    </style>

</head>

<body>
<div class="container">

    <section id="content">
        <img src="<?php echo base_url(); ?>assets/img/logo copy.png" style="width: 374px;
    height: 191px;">
        <form role="form" method="post" action="<?php echo base_url(); ?>login/LoginController/proses_login">
            <h1>Login Form</h1>
            <div>
                <input type="text" placeholder="Username" name="nik" required="" id="username" />
            </div>
            <div>
                <input type="password" placeholder="Password" name="password" required="" id="password" />
            </div>
             <div class="alert-danger p-login"><?php echo validation_errors(); ?></div>
            <div>
                <input class="btn-login" type="submit" value="Log in" />
               <!--  <input type="reset" value="reset" /> -->
                <!-- <a href="#">Lost your password?</a>
                <a href="#">Register</a> -->
            </div>
        </form><!-- form -->
        <!-- <div class="button">
           
            <a href="#">Download source file</a>
        </div> --><!-- button
    </section><!-- content -->
</div><!-- container -->
</body>

</html>
